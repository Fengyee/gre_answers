var fs = require('fs')
var csv = require('csv-parser')
var http = require('http')

var port = 8000
var ip = 'localhost'
if (process.argv.length == 3) {
  ip = process.argv[2]
}

http.createServer(function (req, res) {
  res.writeHead(200, {'Content-Type': 'text/html'})
  var questions = []

  var requests = req.url.substr(1).split('/')

  if (requests.length > 0 && requests[0] == 'list') {
    fs.readdir('.', (err, files) => {
      if (err) {
        console.error(err)
        res.end()
      }

      lists = []
      files.map((f) => {
        if (f.endsWith('.csv')) {
          lists.push(f.slice(0, -4))
        }
      })
      var list_str = ''
      for (i in lists) {
        list_str += `<li><a href='http://${ip}:${port}/${lists[i]}/1'>${lists[i]}</a></li>`
      }

      var content = `
      <!DOCTYPE html>
        <html>
        <head>
        <meta charset="UTF-8">
        <title>GRE Answers</title>
        <style>
          p, pre {
            font-size: 20px
          }
        </style>
        </head>

        <body>
        <div id='content'>
        <ul>
        ${list_str}
        </ul>
        </div>
        </body>

        </html>
      `

      res.write(content)
      res.end()
    })
  }

  if (requests.length == 2) {
    fs.createReadStream(requests[0]+'.csv')
    .pipe(csv({ separator: ' ' }))
    .on('data', function(data) {
      try {
        questions.push(data)
      }
      catch(err) {
        console.error(err)
      }
    })
    .on('end', function () {
      var start_question = parseInt(requests[1])
      console.log('fs read stream end')
      var content = `
        <!DOCTYPE html>
        <html>
        <head>
        <meta charset="UTF-8">
        <title>GRE Answers</title>
        <style>
          p, pre {
            font-size: 20px
          }
        </style>
        </head>

        <body>
        <div id='content'></div>
        <button id='next'>Next</button>

        <script>
          var content = document.getElementById('content')
          var questions = ${JSON.stringify(questions)}
          var question_num = ${start_question}

          var answers = '<ol style="margin-left:50px">'
          questions[question_num-1]['Answer'].split('/').map( s => {answers += '<li><span style="filter: blur(6px)" onclick="javascript: this.style=null">'+s+'</span></li>'} )
          answers += '</ol>'

          content.innerHTML = "<p>Qestion  " + question_num + "</p><pre>Answers: " + answers + "</pre>"

          var next_btn = document.getElementById('next')
          next_btn.addEventListener('click', function (e) {
            question_num += 1
            if (question_num > questions.length) {
              content.innerHTML = "Finished"
            } else {
              answers = '<ol style="margin-left:50px">'
              questions[question_num-1]['Answer'].split('/').map( s => {answers += '<li><span style="filter: blur(6px)" onclick="javascript: this.style=null">'+s+'</span></li>'} )
              answers += '</ol>'
              content.innerHTML = "<p>Qestion  " + question_num + "</p><pre>Answers: " + answers + "</pre>"
            }
          })
        </script>
        </body>

        </html>
      `
      res.write(content)
      res.end()
    })
  }
}).listen(port, '0.0.0.0')

