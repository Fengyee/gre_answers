import requests
from bs4 import BeautifulSoup

words = ["exuberant","abhorrent","detestation","magnanimity","detachment","reprehend","resuscitate","upend","phlegmatic","amenable","caricature","overhaul","flummery","fanatical","impasse","pretension","promulgate","relinquish","proscriptive","spectacular","bludgeon","opinionated","braggart","mawkish","prudent","stridency","rhetoric","wane","palliate","damp","succour","gild","tart","effusive","audacious","rabid","clement","xenophobic","anarchistic","badger","dormant","nugatory","bookish","sensuality","tedium","sententious","exorbitance","orchard","repugnant","pugnacious","execrate","monstrous","demarcate","heresy","apocryphal","superannuation","rejuvenation","senescence","awash","voracious","omnivorous","prodigious","excoriate","temptation","limp","mores","compassion","shrewdness","soft-pedal","distend","putative","venality","grouch","probity","pertinacity","perforce","accretion","anathematic","unpropitious","choreographed","rattle","unrelenting","relent","panegyrics","lowly","calumny","fulmination","fusty","forswear","protean","girth","jibe","avid","purview","contingency","raillery","lugubrious","madcap","lurk","disposition","contrail","multitudinous","obtuse","vociferous","cajole","coax","prophylactic","extirpated","inclusiveness","emancipate","factuality","obscurity","fanatical","archetypal","obstinate","staunch","fealty","compassionate","devious","invective","reassure","reprehend","exonerate","strident","strenuous","irresistible","solace","liable","abet","wondrous","erratic","outlandish","consternation","equanimity","disquieting","credulity","forsaken","eulogized","enviable","surmise","gourmet","stiff","contravene","dismantle","testament","turbid","brackish","fathomless","quell","lingering","illicit","apprehension","maverick","malefactor","paragon","instigation","leniency","oblivious","formidable","stalemate","heft","heedless","untrammelled","exiguity","hasty","flaccid","reparation","denunciation","pernicious","outmaneuver","coerce","expedition","cranky","utterance","colloquial","insurrection","objectionable","cronyism","cavilling","appropriation","unnerved","entanglement","abomination","retrograde","grotesque","pliable","preordain","shoddy","mesmerizing","elegiac","anathema","curt","long-winded","implacable","ingratiating","vitriolic","unctuous","self-effacing","panegyric","vainglorious","nuanced","pry","preempted","topple","traumatic","dismal","earnest","rigor","corroboration","suspense","gadfly","pandemonium","foment","far-fetched","dub","magnanimity","profligacy","demur","expatriate","qualm","scornful","deferential","epitome","snobbish","collegial","perspicacity","unrelenting","extirpate","elation","unqualified","crabbed","indecorous","pedigree","anatomize","fickleness","incorporate","hallowed","quell","inchoate","ungainly","preternatural","prodigious","derogatory","boon","soothsayer","evangelist","recalcitrant","agreeable","obtuse","apparel","ascension","futurity","staggering","imperilled","obedient","inoculate","salvational","tendentious","leaven","disparage","charlatan","malingering","retribution","earnestness","halcyon","aphorism","riotous","superlative","dismantles","corral","encroachment","foray","peninsula","subsistence","wailing","weeping","contrarian","garment","pastoralism","procurement","trite","denigrate","rancorous","parley","immolate","exterminate","strain","parochial","posterity","levity","impecunious","multifarious","subdued","subdue","haziness","exultant","reclusive","recollection","poignant","sensationalized","sanitized","spectacle","distracted","inquisitive","intimidating","dreary","scant","traction","garner","oblivious","prerogative","abet","disposed","reinstate","calumny","blandishment","vituperation","prurience","defamation","hypostatize","abjure","evince","mishap","philistine","dilettante","desiccate","bourgeoisie","machination","atrocious","cogent","vagary","imperturbable","unruffled","arch","idyllic","lingering","incipient"]

results = ""
for word in words:
    result = "<h3>" + word + "</h3>"

    r = requests.get("http://dict.youdao.com/w/" + word)
    if r.status_code == 200:
        soup = BeautifulSoup(r.text, 'html.parser')

        div = soup.find('div', {"class": "trans-container"})
        if div:
            if div.ul:
                result += "<strong>" + div.ul.get_text().strip() + "</strong>\n\n"
            else:
                print(word)
                continue
        else:
            print(word)
            continue
    else:
        print(word)
        continue

    r = requests.get("https://www.merriam-webster.com/dictionary/" + word)
    if r.status_code == 200:
        soup = BeautifulSoup(r.text, 'html.parser')

        div = soup.find('div', {"id": "dictionary-entry-1"})
        if div:
            for index, sense in enumerate(div.find_all('span', {"class": "dtText"})):
                result += "<strong>" + (str(index + 1) + " " + sense.get_text().strip()) + "</strong>\n\n"
        else:
            print(word)
            continue

        div = soup.find('div', {"id": "synonyms-anchor"})
        if div:
            result += "<i>Synonyms: "
            synonyms = div.find('p', {"class": None})
            for synonym in synonyms.find_all('a'):
                result += synonym.get_text().strip() + "; "
            result += "</i>\n\n"
    else:
        print(word)
        continue
    result += "***\n\n"
    results += result

with open('./vocab_search_result.md', 'w+') as f:
    f.write(results)
