<h3>exuberant</h3><strong>adj. 精力充沛的，热情洋溢的；兴高采烈的；繁茂的，茂盛的；充满生气的，鲜艳的；充满活力和想象力的</strong>

<strong>1 : joyously unrestrained and enthusiastic

exuberant praise an exuberant personality</strong>

<strong>2 : unrestrained or elaborate especially in style : flamboyant
exuberant architecture</strong>

<strong>3 : produced in extreme abundance : plentiful
exuberant foliage and vegetation</strong>

<strong>4 : extreme or excessive in degree, size, or extent
exuberant prosperity</strong>

<i>Synonyms: bouncy; bubbly; buoyant; crank; effervescent; frolic; frolicsome; gamesome; gay; high-spirited; vivacious; </i>

***

<h3>abhorrent</h3><strong>adj. 可恶的；厌恶的；格格不入的</strong>

<strong>1 : causing or deserving strong dislike or hatred : being so repugnant as to stir up positive antagonism

acts abhorrent to every right-minded person</strong>

<strong>2 : not agreeable : contrary
a notion abhorrent to their philosophy</strong>

<strong>3 : feeling or showing strong dislike or hatred</strong>

<strong>4 : strongly opposed</strong>

<i>Synonyms: abominable; appalling; awful; disgusting; distasteful; dreadful; evil; foul; fulsome; gross; hideous; horrendous; horrible; horrid; loathsome; nasty; nauseating; nauseous; noisome; noxious; obnoxious; obscene; odious; offensive; rancid; repellent; repellant; repugnant; repulsive; revolting; scandalous; shocking; sickening; ugly; </i>

***

<h3>detestation</h3><strong>n. 嫌恶，痛恨，憎恶；令人厌恶的人</strong>

<strong>1 : extreme hatred or dislike : abhorrence, loathing</strong>

<strong>2 : an object of hatred or contempt</strong>

<i>Synonyms: abhorrence; abomination; execration; hate; hatred; loathing; </i>

***

<h3>magnanimity</h3><strong>n. 宽宏大量；慷慨</strong>

<strong>1 : the quality of being magnanimous : loftiness of spirit enabling one to bear trouble calmly, to disdain meanness and pettiness, and to display a noble generosity

He had the magnanimity to forgive her for lying about him.</strong>

<strong>2 : a magnanimous act

the great magnanimities between soldiers— Katharine Tynan</strong>

***

<h3>detachment</h3><strong>n. 分离，拆开；超然；分遣；分遣队</strong>

<strong>1 : the action or process of detaching : separation</strong>

<strong>2 : the dispatch of a body of troops or part of a fleet from the main body for a special mission or service</strong>

<strong>3 : the part so dispatched</strong>

<strong>4 : a permanently organized separate unit usually smaller than a platoon and of special composition</strong>

<strong>5 : indifference to worldly concerns : aloofness</strong>

<strong>6 : freedom from bias or prejudice</strong>

<i>Synonyms: disinterest; disinterestedness; equity; evenhandedness; fair-mindedness; fairness; impartiality; justice; neutralism; neutrality; nonpartisanship; objectiveness; objectivity; </i>

***

<h3>reprehend</h3><strong>vt. 申斥；指责
n. 责备</strong>

<strong>1 : to voice disapproval of : censure</strong>

<i>Synonyms: anathematize; censure; condemn; damn; decry; denounce; execrate; reprobate; </i>

***

<h3>resuscitate</h3><strong>vt. 使复苏；使复兴
vi. 恢复；复兴</strong>

<strong>1 : to revive from apparent death or from unconsciousness</strong>

<strong>2 : revitalize</strong>

<strong>3 : come to, revive</strong>

<i>Synonyms: freshen; recharge; recreate; refresh; refreshen; regenerate; rejuvenate; renew; repair; restore; revitalize; revive; revivify; </i>

***

<h3>upend</h3><strong>vt. 倒放，颠倒；竖立</strong>

<strong>1 : to set or stand on end</strong>

<strong>2 : overturn sense 1</strong>

<strong>3 : to affect to the point of being upset or flurried

a … literary shocker, designed to upend the credulous matrons— Wolcott Gibbs</strong>

<strong>4 : defeat, beat</strong>

<strong>5 : to rise on an end</strong>

<i>Synonyms: beat; best; conquer; defeat; dispatch; do down; get; get around; lick; master; overbear; overcome; overmatch; prevail (over); skunk; stop; subdue; surmount; take; trim; triumph (over); win (against); worst; </i>

***

<h3>phlegmatic</h3><strong>adj. 冷淡的；迟钝的；冷漠的</strong>

<strong>1 : resembling, consisting of, or producing the humor phlegm</strong>

<strong>2 : having or showing a slow and stolid temperament</strong>

<i>Synonyms: affectless; apathetic; cold-blooded; emotionless; impassible; impassive; numb; passionless; stoic; stoical; stolid; undemonstrative; unemotional; </i>

***

<h3>amenable</h3><strong>adj. 有责任的：顺从的，服从的；有义务的；经得起检验的</strong>

<strong>1 : liable to be brought to account : answerable
citizens amenable to the law</strong>

<strong>2 : capable of submission (as to judgment or test) : suited
The data is amenable to analysis.</strong>

<strong>3 : readily brought to yield, submit, or cooperate

a government not amenable to change</strong>

<strong>4 : willing sense 1
was amenable to spending more time at home</strong>

<i>Synonyms: disposed; fain; game; glad; inclined; minded; ready; willing; </i>

***

<h3>caricature</h3><strong>n. 人物漫画；夸张的描述；漫画艺术，漫画手法；讽刺描述法；（人或物的）可笑（或怪诞）样式
v.把……画成漫画；滑稽地描述，使滑稽化</strong>

<strong>1 : exaggeration by means of often ludicrous distortion of parts or characteristics

drew a caricature of the president</strong>

<strong>2 : a representation especially in literature or art that has the qualities of caricature

His performance in the film was a caricature of a hard-boiled detective.</strong>

<strong>3 : a distortion so gross as to seem like caricature

The kangaroo court was a caricature of justice.</strong>

<i>Synonyms: cartoon; farce; joke; mockery; parody; sham; travesty; </i>

***

<h3>overhaul</h3><strong>v. 彻底检修；全面改革（制度、方法等）；赶上（赛跑对手）
n. （机器的）解体检修；（系统、体制等的）全面修改</strong>

<strong>1 : to examine thoroughly

our systems of education are being constantly overhauled — Saturday Rev.</strong>

<strong>2 : repair
The mechanic overhauled the engine.</strong>

<strong>3 : to renovate, remake, revise, or renew thoroughly

Lawmakers are overhauling the welfare program.</strong>

<strong>4 : to haul or drag over</strong>

<strong>5 : overtake
The most imposing U.S. swimmer was overhauled by a 17-year-old Australian in the butterfly race.</strong>

<i>Synonyms: catch; catch up (with); overtake; </i>

***

<h3>flummery</h3><strong>n. 假恭维；柔软易食的食品</strong>

<strong>1 : a soft jelly or porridge made with flour or meal</strong>

<strong>2 : any of several sweet desserts</strong>

<strong>3 : mummery, mumbo jumbo</strong>

***

<h3>fanatical</h3><strong>adj. 狂热的</strong>

<strong>1 : a person exhibiting excessive enthusiasm and intense uncritical devotion toward some controversial matter (as in religion or politics)

a religious fanatic [=extremist] The fanatics are convinced they are serving a righteous cause and that all means are justified …— Flora Lewis</strong>

<strong>2 : a person who is extremely enthusiastic about and devoted to some interest or activity

a boating/sports/racing fanatic She's a real fanatic when it comes to working out. Since the U.S. economy began to sputter in 2008, shoppers have become coupon fanatics and lovers of buy-one-get-one-free deals …— Janet K. Keeler</strong>

<i>Synonyms: addict; aficionado; afficionado; buff; bug; devotee; enthusiast; fan; fancier; fiend; fool; freak; habitué; habitue; head; hound; junkie; junky; lover; maniac; maven; mavin; nut; sucker; </i>

***

<h3>impasse</h3><strong>n. 僵局；死路</strong>

<strong>1 : a predicament affording no obvious escape</strong>

<strong>2 : deadlock</strong>

<strong>3 : an impassable road or way : cul-de-sac</strong>

<i>Synonyms: deadlock; gridlock; halt; logjam; Mexican standoff; stalemate; standoff; standstill; </i>

***

<h3>pretension</h3><strong>n. 自负；要求；主张；借口；骄傲</strong>

<strong>1 : an allegation of doubtful value : pretext</strong>

<strong>2 : a claim or an effort to establish a claim</strong>

<strong>3 : a claim or right to attention or honor because of merit</strong>

<strong>4 : an aspiration or intention that may or may not reach fulfillment

has serious literary pretensions</strong>

<strong>5 : vanity, pretentiousness</strong>

<i>Synonyms: call; claim; dibs; pretense; pretence; right; </i>

***

<h3>promulgate</h3><strong>vt. 公布；传播；发表</strong>

<strong>1 : to make (an idea, belief, etc.) known to many people by open declaration : proclaim
… the huge meeting served primarily as the occasion on which to promulgate the official doctrine …— Roger Shattuck From the beginning our objective has been to develop and promulgate new models for the calculus-based introductory course.— John S. Rigden et al.</strong>

<strong>2 : to make known or public the terms of (a proposed law)

The law was promulgated in February 1993.</strong>

<strong>3 : to put (a law or rule) into action or force

… more than 200 colleges and universities have promulgated behavioral codes that punish various forms of harassment …— Ken Myers</strong>

<i>Synonyms: advertise; announce; annunciate; blare; blaze; blazon; broadcast; declare; enunciate; flash; give out; herald; placard; post; proclaim; publicize; publish; release; sound; trumpet; </i>

***

<h3>relinquish</h3><strong>vt. 放弃；放手；让渡</strong>

<strong>1 : to withdraw or retreat from : leave behind</strong>

<strong>2 : give up
relinquish a title</strong>

<strong>3 : to stop holding physically : release
slowly relinquished his grip on the bar</strong>

<strong>4 : to give over possession or control of : yield
few leaders willingly relinquish power</strong>

<i>Synonyms: cede; cough up; deliver; give up; hand over; lay down; render; surrender; turn in; turn over; yield; </i>

***

<h3>proscriptive</h3><strong>adj. 放逐的；剥夺人权的；禁止的</strong>

<strong>1 : the act of proscribing : the state of being proscribed</strong>

<strong>2 : an imposed restraint or restriction : prohibition</strong>

<i>Synonyms: banning; barring; enjoining; forbidding; interdicting; interdiction; outlawing; prohibiting; prohibition; proscribing; </i>

***

<h3>spectacular</h3><strong>adj. 壮观的，惊人的；公开展示的</strong>

<strong>1 : of, relating to, or being a spectacle : striking, sensational
a spectacular display of fireworks</strong>

<i>Synonyms: circus; extravaganza; pageant; raree-show; spectacle; </i>

***

<h3>bludgeon</h3><strong>vt. 恫吓；用棍棒打；猛烈攻击
n. 棍棒；攻击
vi. 恫吓；用棍棒打；猛烈攻击</strong>

<strong>1 : a short stick that usually has one thick or loaded end and is used as a weapon</strong>

<strong>2 : something used to attack or bully

the bludgeon of satire</strong>

<i>Synonyms: bastinado; bastinade; bat; baton; billy; billy club; cane; club; cudgel; nightstick; rod; rung; sap; shillelagh; shillalah; staff; truncheon; waddy; </i>

***

<h3>opinionated</h3><strong>adj. 固执己见的；武断的</strong>

<strong>1 : firmly or unduly adhering to one's own opinion or to preconceived notions

… focus groups, which tend to be dominated by the loudest and most opinionated people …— James Surowiecki</strong>

<i>Synonyms: doctrinaire; dogmatic; dogmatical; opinionative; opinioned; pontifical; self-opinionated; </i>

***

<h3>braggart</h3><strong>n. 吹嘘；好自夸者；大言者
adj. 吹牛的；自夸的</strong>

<strong>1 : a loud arrogant boaster

thinks he's a loudmouth braggart</strong>

<i>Synonyms: blower; blowhard; boaster; brag; braggadocio; bragger; cockalorum; cracker; gascon; gasconader; swaggerer; vaunter; </i>

***

<h3>mawkish</h3><strong>adj. 令人作呕的，令人厌恶的；自作多情的；淡而无味的</strong>

<strong>1 : lacking flavor or having an unpleasant taste</strong>

<strong>2 : exaggeratedly or childishly emotional

a mawkish love story mawkish poetry</strong>

<i>Synonyms: chocolate-box; cloying; corny; drippy; fruity; gooey; lovey-dovey; maudlin; mushy; novelettish; saccharine; sappy; schmaltzy; sentimental; sloppy; slushy; soppy; soupy; spoony; spooney; sticky; sugarcoated; sugary; wet; </i>

***

<h3>prudent</h3><strong>adj. 谨慎的；精明的；节俭的
n. (Prudent)人名；(法)普吕当</strong>

<strong>1 : characterized by, arising from, or showing prudence: such as</strong>

<strong>2 : marked by wisdom or judiciousness

prudent advice</strong>

<strong>3 : shrewd in the management of practical affairs

prudent investors</strong>

<strong>4 : marked by circumspection : discreet</strong>

<strong>5 : provident, frugal</strong>

<i>Synonyms: discreet; intelligent; judgmatic; judgmatical; judicious; </i>

***

<h3>stridency</h3><strong>n. 刺耳；尖锐</strong>

<strong>1 : the quality or state of being strident</strong>

***

<h3>rhetoric</h3><strong>n. 修辞，修辞学；华丽的词藻
adj. 花言巧语的</strong>

<strong>1 : the art of speaking or writing effectively: such as</strong>

<strong>2 : the study of principles and rules of composition formulated by critics of ancient times</strong>

<strong>3 : the study of writing or speaking as a means of communication or persuasion</strong>

<strong>4 : skill in the effective use of speech</strong>

<strong>5 : a type or mode of language or speech</strong>

<strong>6 : insincere or grandiloquent language</strong>

<strong>7 : verbal communication : discourse</strong>

<i>Synonyms: bombast; fustian; gas; grandiloquence; hot air; oratory; verbiage; wind; </i>

***

<h3>wane</h3><strong>vi. 衰落；变小；亏缺；退潮；消逝
n. 衰退；月亏；衰退期；缺损
n. (Wane)人名；(英)韦恩；(阿拉伯)瓦尼；(几、马里、塞内)瓦内</strong>

<strong>1 : to decrease in size, extent, or degree : dwindle: such as</strong>

<strong>2 : to diminish in phase or intensity

—used chiefly of the moon, other satellites, and inferior planets</strong>

<strong>3 : to become less brilliant or powerful : dim</strong>

<strong>4 : to flow out : ebb</strong>

<strong>5 : to fall gradually from power, prosperity, or influence</strong>

<i>Synonyms: abate; de-escalate; decline; decrease; die (away or down or out); diminish; drain (away); drop (off); dwindle; ease; ebb; fall; fall away; lessen; let up; lower; moderate; pall; phase down; ratchet (down); rachet (down); recede; relent; remit; shrink; subside; taper; taper off; </i>

***

<h3>palliate</h3><strong>vt. 减轻；掩饰；辩解</strong>

<strong>1 : to reduce the violence of (a disease)</strong>

<strong>2 : to ease (symptoms) without curing the underlying disease</strong>

<i>Synonyms: deodorize; excuse; explain away; extenuate; gloss (over); gloze (over); whitewash; </i>

***

<h3>damp</h3><strong>adj. 潮湿的
n. 潮湿；气馁；沼气
v. （使）潮湿；（使）沮丧，抑制；灭火；限止（钢琴或其他乐器琴弦）的音；减幅</strong>

<strong>1 : a noxious gas— compare black damp, firedamp</strong>

<strong>2 : moisture:</strong>

<strong>3 : humidity, dampness</strong>

<strong>4 : fog, mist</strong>

<strong>5 : discouragement, check</strong>

<strong>6 : depression, dejection</strong>

<i>Synonyms: dampness; humidity; moistness; moisture; </i>

***

<h3>succour</h3><strong>n. 救援物品
vt. 救助</strong>

***

<h3>gild</h3><strong>vt. 镀金；虚饰；供给钱
n. (Gild)人名；(俄)希尔德</strong>

<strong>1 : to overlay with or as if with a thin covering of gold</strong>

<strong>2 : to give money to</strong>

<strong>3 : to give an attractive but often deceptive appearance to

was hired to gild the company's image</strong>

<strong>4 : to make bloody</strong>

<strong>5 : to add unnecessary ornamentation to something beautiful in its own right</strong>

***

<h3>tart</h3><strong>adj. 酸的；锋利的；尖刻的
n. 果馅饼；妓女
vt. 打扮
n. (Tart)人名；(英)塔特</strong>

<strong>1 : agreeably sharp or acid to the taste

a tart apple</strong>

<strong>2 : marked by a biting, acrimonious, or cutting quality

a tart rejoinder</strong>

<i>Synonyms: acid; acidic; acidulous; sour; sourish; tartish; vinegary; </i>

***

<h3>effusive</h3><strong>adj. 流露感激之情的；（火山岩）喷发的；（岩浆）大量喷发的</strong>

<strong>1 : marked by the expression of great or excessive emotion or enthusiasm

effusive praise</strong>

<strong>2 : pouring freely</strong>

<strong>3 : characterized or formed by a nonexplosive outpouring of lava

effusive rocks</strong>

<i>Synonyms: demonstrative; emotional; touchy-feely; uninhibited; unreserved; unrestrained; </i>

***

<h3>audacious</h3><strong>adj. 无畏的；鲁莽的</strong>

<strong>1 : intrepidly daring : adventurous
an audacious mountain climber</strong>

<strong>2 : recklessly bold : rash
an audacious maneuver</strong>

<strong>3 : contemptuous of law, religion, or decorum : insolent
an audacious maverick</strong>

<strong>4 : marked by originality and verve
audacious experiments</strong>

<i>Synonyms: arch; bold; bold-faced; brash; brassbound; brassy; brazen; brazen-faced; cheeky; cocksure; cocky; fresh; impertinent; impudent; insolent; nervy; sassy; saucy; wise; </i>

***

<h3>rabid</h3><strong>adj. 激烈的；狂暴的；偏激的；患狂犬病的</strong>

<strong>1 : extremely violent : furious</strong>

<strong>2 : going to extreme lengths in expressing or pursuing a feeling, interest, or opinion

rabid editorials a rabid supporter</strong>

<strong>3 : affected with rabies</strong>

<i>Synonyms: extreme; extremist; fanatic; fanatical; radical; revolutionary; revolutionist; ultra; </i>

***

<h3>clement</h3><strong>adj. （天气）温和的；（人、行为）仁慈的</strong>

<strong>1 : inclined to be merciful : lenient
a clement judge</strong>

<strong>2 : mild
clement weather for this time of year</strong>

<i>Synonyms: balmy; equable; genial; gentle; mild; moderate; soft; temperate; </i>

***

<h3>xenophobic</h3><strong>adj. 恐惧外国人的</strong>

<strong>1 : one unduly fearful of what is foreign and especially of people of foreign origin</strong>

***

<h3>anarchistic</h3><strong>adj. 无政府主义的</strong>

<strong>1 : a person who rebels against any authority, established order, or ruling power</strong>

<strong>2 : a person who believes in, advocates, or promotes anarchism or anarchy</strong>

<strong>3 : one who uses violent means to overthrow the established order</strong>

***

<h3>badger</h3><strong>n. 獾；獾皮；（大写）獾州人（美国威斯康星州人的别称）；<动><澳>毛鼻袋熊
vt. 纠缠不休；吵着要；烦扰
n. (Badger)人名；(英)巴杰</strong>

<strong>1 : any of various burrowing mammals (especially Taxidea taxus and Meles meles) of the weasel family that are widely distributed in the northern hemisphere</strong>

<strong>2 : the pelt or fur of a badger</strong>

<strong>3 : a native or resident of Wisconsin

—used as a nickname</strong>

***

<h3>dormant</h3><strong>adj. 休眠的；静止的；睡眠状态的；隐匿的
n. (Dormant)人名；(法)多尔芒</strong>

<strong>1 : represented on a coat of arms in a lying position with the head on the forepaws</strong>

<strong>2 : marked by a suspension of activity: such as</strong>

<strong>3 : temporarily devoid of external activity

a dormant volcano</strong>

<strong>4 : temporarily in abeyance yet capable of being activated

seeds will remain dormant until spring reawaken her dormant emotions</strong>

<strong>5 : asleep, inactive
dormant creatures</strong>

<strong>6 : having the faculties suspended : sluggish</strong>

<strong>7 : having biological activity suspended: such as</strong>

<strong>8 : being in a state of suspended animation</strong>

<strong>9 : not actively growing but protected (as by bud scales) from the environment

—used of plant parts</strong>

<strong>10 : associated with, carried out, or applied during a period of dormancy
dormant grafting</strong>

<i>Synonyms: asleep; dozing; napping; resting; sleeping; slumbering; </i>

***

<h3>nugatory</h3><strong>adj. 无价值的；琐碎的；无效的；无法律效力的</strong>

<strong>1 : of little or no consequence : trifling, inconsequential
comments too nugatory to merit attention</strong>

<strong>2 : having no force : inoperative
The law was unenforced and thus rendered nugatory.</strong>

<i>Synonyms: bad; inoperative; invalid; nonbinding; nonvalid; null; null and void; void; </i>

***

<h3>bookish</h3><strong>adj. 书本上的；好读书的；书呆子气的</strong>

<strong>1 : of or relating to books</strong>

<strong>2 : fond of books and reading</strong>

<strong>3 : inclined to rely on book knowledge</strong>

<strong>4 : literary and formal as opposed to colloquial and informal</strong>

<strong>5 : given to literary or scholarly pursuits</strong>

<strong>6 : affectedly learned</strong>

<i>Synonyms: erudite; learned; literary; </i>

***

<h3>sensuality</h3><strong>n. 身体享受（尤指性快感）；感官享受</strong>

<strong>1 : relating to or consisting in the gratification of the senses or the indulgence of appetite : fleshly</strong>

<strong>2 : sensory sense 1</strong>

<strong>3 : devoted to or preoccupied with the senses or appetites</strong>

<strong>4 : voluptuous</strong>

<strong>5 : deficient in moral, spiritual, or intellectual interests : worldly</strong>

<strong>6 : irreligious</strong>

<i>Synonyms: carnal; fleshly; luscious; lush; sensuous; voluptuous; </i>

***

<h3>tedium</h3><strong>n. 沉闷；单调乏味；厌烦</strong>

<strong>1 : the quality or state of being tedious : tediousness</strong>

<strong>2 : boredom</strong>

<strong>3 : a tedious period of time</strong>

<i>Synonyms: blahs; boredom; doldrums; ennui; listlessness; restlessness; weariness; </i>

***

<h3>sententious</h3><strong>adj. 简洁精炼的；警句的，富于格言的</strong>

<strong>1 : given to or abounding in aphoristic expression</strong>

<strong>2 : given to or abounding in excessive moralizing</strong>

<strong>3 : terse, aphoristic, or moralistic in expression : pithy, epigrammatic</strong>

<i>Synonyms: didactic; homiletic; homiletical; moralistic; moralizing; preachy; sermonic; </i>

***

<h3>exorbitance</h3><strong>n. 过度；不当</strong>

<strong>1 : an exorbitant action or procedure</strong>

<strong>2 : excessive or gross deviation from rule, right, or propriety</strong>

<strong>3 : the tendency or disposition to be exorbitant</strong>

<i>Synonyms: excess; excessiveness; immoderacy; immoderation; insobriety; intemperance; intemperateness; nimiety; </i>

***

<h3>orchard</h3><strong>n. 果园；果树林
n. (Orchard)人名；(英、西)奥查德</strong>

<strong>1 : a planting of fruit trees, nut trees, or sugar maples</strong>

<strong>2 : the trees of such a planting</strong>

***

<h3>repugnant</h3><strong>adj. 使人极度反感的，难以接受的；与……矛盾，与……不相一致</strong>

<strong>1 : incompatible, inconsistent</strong>

<strong>2 : hostile</strong>

<strong>3 : exciting distaste or aversion

repugnant language a morally repugnant practice</strong>

<i>Synonyms: abhorrent; abominable; appalling; awful; disgusting; distasteful; dreadful; evil; foul; fulsome; gross; hideous; horrendous; horrible; horrid; loathsome; nasty; nauseating; nauseous; noisome; noxious; obnoxious; obscene; odious; offensive; rancid; repellent; repellant; repulsive; revolting; scandalous; shocking; sickening; ugly; </i>

***

<h3>pugnacious</h3><strong>adj. 好斗的，好战的</strong>

<strong>1 : having a quarrelsome or combative nature : truculent</strong>

<i>Synonyms: aggressive; agonistic; argumentative; assaultive; bellicose; belligerent; brawly; chippy; combative; confrontational; contentious; discordant; disputatious; feisty; gladiatorial; militant; quarrelsome; scrappy; truculent; warlike; </i>

***

<h3>execrate</h3><strong>v. 憎恶；痛骂；诅咒</strong>

<strong>1 : to declare to be evil or detestable : denounce</strong>

<strong>2 : to detest utterly</strong>

<i>Synonyms: anathematize; censure; condemn; damn; decry; denounce; reprehend; reprobate; </i>

***

<h3>monstrous</h3><strong>adj. 巨大的；怪异的；荒谬的；畸形的</strong>

<strong>1 : strange, unnatural</strong>

<strong>2 : having extraordinary often overwhelming size : gigantic
stuffing a monstrous sandwich down his throat— Mike Flaherty a monstrous skyscraper</strong>

<strong>3 : having the qualities or appearance of a monster
came face to face with a monstrous creature— Lester David</strong>

<strong>4 : teeming with monsters</strong>

<strong>5 : extraordinarily ugly or vicious : horrible
a monstrous crime monstrous mayhem</strong>

<strong>6 : shockingly wrong or ridiculous

the legend assumed monstrous proportions— Louis Untermeyer a monstrous miscalculation</strong>

<strong>7 : deviating greatly from the natural form or character : abnormal
a monstrous melon</strong>

<strong>8 : very great

—used as an intensive a monstrous hammering on his door— G. D. Browna monstrous hangover</strong>

<i>Synonyms: deformed; distorted; malformed; misshapen; shapeless; </i>

***

<h3>demarcate</h3><strong>vt. 划分界线；区别</strong>

<strong>1 : delimit
a plot of land demarcated by a low stone wall</strong>

<strong>2 : to set apart : distinguish
demarcate teachers as mentor, master and model teachers based on their level of education— Shanay Cadette</strong>

<i>Synonyms: bound; circumscribe; define; delimit; demark; limit; mark (off); terminate; </i>

***

<h3>heresy</h3><strong>n. 异端；异端邪说；异教</strong>

<strong>1 : adherence to a religious opinion contrary to church dogma (see dogma sense 2)

They were accused of heresy.</strong>

<strong>2 : denial of a revealed truth by a baptized member of the Roman Catholic Church</strong>

<strong>3 : an opinion or doctrine contrary to church dogma</strong>

<strong>4 : dissent or deviation from a dominant theory, opinion, or practice

To disagree with the party leadership was heresy.</strong>

<strong>5 : an opinion, doctrine, or practice contrary to the truth or to generally accepted beliefs or standards

our democratic heresy which holds that … truth is to be found by majority vote— M. W. Straight</strong>

<i>Synonyms: dissent; dissidence; heterodoxy; nonconformity; </i>

***

<h3>apocryphal</h3><strong>adj. 伪的；可疑的</strong>

<strong>1 : of doubtful authenticity : spurious
an apocryphal story about George Washington</strong>

<strong>2 : of or resembling the Apocrypha
Apocryphal books of the Old Testament</strong>

***

<h3>superannuation</h3><strong>n. 退休；退休金；废弃；陈旧</strong>

<strong>1 : to make, declare, or prove obsolete or out-of-date</strong>

<strong>2 : to retire and pension because of age or infirmity</strong>

<strong>3 : to become retired</strong>

<strong>4 : to become antiquated</strong>

***

<h3>rejuvenation</h3><strong>n. [地质][水文] 回春，返老还童；复壮，恢复活力</strong>

<strong>1 : the action of rejuvenating or the state of being rejuvenated : restoration of youthful vigor

I remind myself that it is hope for rejuvenation of body and spirit after the rigors of the holidays that has brought me to the brink…— Jeff Phillips Europeans insist that their nations are not "immigration" countries, as is the United States, which by … tradition views immigrants as a source of cultural richness and economic rejuvenation.— Judith Miller rejuvenation of streams … formed his ideas and planned his schemes for the rejuvenation of the drama …— Arnold Bennett … poured half a billion dollars into Italy's post-war rejuvenation …— Arthur Vandenberg</strong>

<i>Synonyms: reanimation; rebirth; regeneration; rejuvenescence; renewal; resurgence; resurrection; resuscitation; revitalization; revival; revivification; </i>

***

<h3>senescence</h3><strong>n. 衰老</strong>

<strong>1 : the state of being old : the process of becoming old</strong>

<strong>2 : the growth phase in a plant or plant part (such as a leaf) from full maturity to death</strong>

***

<h3>awash</h3><strong>adj. 被浪冲打的；与水面齐平的；充斥的</strong>

<strong>1 : alternately covered and exposed by waves or tide</strong>

<strong>2 : washing about : afloat</strong>

<strong>3 : covered with water : flooded</strong>

<strong>4 : filled, covered, or completely overrun as if by a flood

a movie awash in sentimentality</strong>

<i>Synonyms: bathed; bedraggled; doused; dowsed; drenched; dripping; logged; saturate; saturated; soaked; soaking; sodden; soggy; sopping; soppy; soused; washed; water-soaked; watered; waterlogged; watery; wet; </i>

***

<h3>voracious</h3><strong>adj. 贪婪的；贪吃的；狼吞虎咽的</strong>

<strong>1 : having a huge appetite : ravenous</strong>

<strong>2 : excessively eager : insatiable
a voracious reader</strong>

<i>Synonyms: edacious; esurient; gluttonous; greedy; hoggish; piggish; rapacious; ravenous; swinish; </i>

***

<h3>omnivorous</h3><strong>adj. 杂食的；什么都读的；无所不吃的</strong>

<strong>1 : feeding on both animal and vegetable substances

omnivorous animals</strong>

<strong>2 : avidly taking in everything as if devouring or consuming

an omnivorous reader omnivorous curiosity</strong>

***

<h3>prodigious</h3><strong>adj. 惊人的，异常的，奇妙的；巨大的</strong>

<strong>1 : causing amazement or wonder</strong>

<strong>2 : extraordinary in bulk, quantity, or degree : enormous</strong>

<strong>3 : resembling or befitting a prodigy : strange, unusual</strong>

<strong>4 : being an omen : portentous</strong>

<i>Synonyms: amazing; astonishing; astounding; awesome; awful; eye-opening; fabulous; marvelous; marvellous; miraculous; portentous; staggering; stunning; stupendous; sublime; surprising; wonderful; wondrous; </i>

***

<h3>excoriate</h3><strong>vt. 严厉的责难；擦破...的皮肤</strong>

<strong>1 : to wear off the skin of : abrade</strong>

<strong>2 : to censure scathingly</strong>

<i>Synonyms: abuse; assail; attack; bash; belabor; blast; castigate; jump (on); lambaste; lambast; potshot; savage; scathe; slam; trash; vituperate; </i>

***

<h3>temptation</h3><strong>n. 引诱；诱惑物</strong>

<strong>1 : the act of tempting or the state of being tempted especially to evil : enticement</strong>

<strong>2 : something tempting : a cause or occasion of enticement</strong>

<i>Synonyms: allurement; enticement; lure; seduction; </i>

***

<h3>limp</h3><strong>adj. 柔软的，无力的；软弱的
vi. 跛行，一拐一拐地走；缓慢费力地前进
n. 跛行
n. (Limp)人名；(英)林普</strong>

<strong>1 : to walk lamely</strong>

<strong>2 : to walk favoring one leg</strong>

<i>Synonyms: halt; hobble; </i>

***

<h3>mores</h3><strong>n. 习惯，习俗；风俗；道德观念
n. (Mores)人名；(捷)莫雷斯</strong>

<strong>1 : the fixed morally binding customs of a particular group

have tended to withdraw and develop a self-sufficient society of their own, with distinct and rigid mores— James Stirling</strong>

<strong>2 : moral attitudes

the evershifting mores of the moment— Havelock Ellis</strong>

<strong>3 : habits, manners
organized dancing developed a whole set of mores and practices of its own— R. L. Taylor</strong>

<i>Synonyms: etiquette; form; manner; proprieties; </i>

***

<h3>compassion</h3><strong>n. 同情；怜悯</strong>

<strong>1 : sympathetic consciousness of others' distress together with a desire to alleviate it</strong>

<i>Synonyms: commiseration; feeling; sympathy; </i>

***

<h3>shrewdness</h3><strong>n. 精明；机灵</strong>

<strong>1 : marked by clever discerning awareness and hardheaded acumen
shrewd common sense</strong>

<strong>2 : given to wily and artful ways or dealing

a shrewd operator</strong>

<strong>3 : severe, hard
a shrewd knock</strong>

<strong>4 : sharp, piercing
a shrewd wind</strong>

<strong>5 : mischievous</strong>

<strong>6 : abusive, shrewish</strong>

<strong>7 : ominous, dangerous</strong>

<i>Synonyms: astute; canny; clear-eyed; clear-sighted; hard-boiled; hardheaded; heady; knowing; savvy; sharp; sharp-witted; smart; </i>

***

<h3>soft-pedal</h3><strong>vt. 用弱音踏板降低…的音调；对…不予张扬；使大事化小
vi. 使用弱音踏板；缓和；不张扬</strong>

<strong>1 : play down, de-emphasize
soft-pedal the issue</strong>

<strong>2 : to use the soft pedal in playing</strong>

<i>Synonyms: de-emphasize; downplay; play down; </i>

***

<h3>distend</h3><strong>vt. 使…膨胀；使…扩张
vi. 扩张；膨胀</strong>

<strong>1 : extend
the main outlines of the land yet lay clearly distended before them— Norman Douglas</strong>

<strong>2 : to enlarge, expand, or stretch out (as from internal pressure) : swell
a distended abdomen</strong>

<strong>3 : to become enlarged, expanded, or stretched out

causing the stomach to distend</strong>

***

<h3>putative</h3><strong>adj. 推定的，假定的</strong>

<strong>1 : commonly accepted or supposed</strong>

<strong>2 : assumed to exist or to have existed</strong>

<i>Synonyms: apparent; assumed; evident; ostensible; ostensive; presumed; prima facie; reputed; seeming; supposed; </i>

***

<h3>venality</h3><strong>n. 唯利是图；贪赃枉法；受贿</strong>

<strong>1 : capable of being bought or obtained for money or other valuable consideration : purchasable</strong>

<strong>2 : open to corrupt influence and especially bribery : mercenary</strong>

<i>Synonyms: bribable; corruptible; dirty; purchasable; </i>

***

<h3>grouch</h3><strong>n. 心怀不满；不高兴的人；抱怨
vi. 发牢骚；闹脾气；抱怨</strong>

<strong>1 : a fit of bad temper</strong>

<strong>2 : grudge, complaint</strong>

<strong>3 : a habitually irritable or complaining person : grumbler</strong>

<i>Synonyms: bear; bellyacher; complainer; crab; crank; croaker; crosspatch; curmudgeon; fusser; griper; grouser; growler; grumbler; grump; murmurer; mutterer; sourpuss; whiner; </i>

***

<h3>probity</h3><strong>n. 廉洁；正直</strong>

<strong>1 : adherence to the highest principles and ideals : uprightness</strong>

<i>Synonyms: character; decency; goodness; honesty; integrity; morality; rectitude; righteousness; rightness; uprightness; virtue; virtuousness; </i>

***

<h3>pertinacity</h3><strong>n. 顽固；执拗</strong>

<strong>1 : adhering resolutely to an opinion, purpose, or design</strong>

<strong>2 : perversely persistent</strong>

<strong>3 : stubbornly tenacious</strong>

<i>Synonyms: dogged; insistent; patient; persevering; persistent; tenacious; </i>

***

<h3>perforce</h3><strong>adv. 一定，必须；必然地</strong>

<strong>1 : by force of circumstances or of necessity

These images are perforce in black and white because there is no color at x-ray wavelengths. — Smithsonian All our perceptions of China are perforce limited, partial, biased by our cultural and political perspectives.— Marilyn B. Young With no new novel in the offing, Harry addicts will perforce focus their anticipation during the coming year on the film version of the first book, Harry Potter and the Sorcerer's Stone …— Paul Gray</strong>

<strong>2 : by physical coercion

… he rushed into my house and took perforce my ring away.— William Shakespeare</strong>

<i>Synonyms: ineluctably; inescapably; inevitably; ipso facto; necessarily; needs; unavoidably; </i>

***

<h3>accretion</h3><strong>n. 积聚层，冲积层；加，积聚（过程）；积淀物，增加物；吸积</strong>

<strong>1 : the process of growth or enlargement by a gradual buildup: such as</strong>

<strong>2 : increase by external addition or accumulation (as by adhesion of external parts or particles)</strong>

<strong>3 : the increase of land by the action of natural forces</strong>

<strong>4 : a product of accretion</strong>

<strong>5 : an extraneous addition</strong>

<i>Synonyms: accumulation; assemblage; collection; cumulation; cumulus; gathering; lodgment; lodgement; pileup; </i>

***

<h3>anathematic</h3><strong>adj. 厌恶的；憎恶的</strong>

<strong>1 : hateful, loathsome</strong>

***

<h3>unpropitious</h3><strong>adj. 不吉利的；不顺遂的</strong>

<strong>1 : not likely to have or produce a good result : not favorable or advantageous : not propitious
an unpropitious time Jasper could not have selected a more unpropitious moment for his cause.— Edward Bulwer-Lytton</strong>

***

<h3>choreographed</h3><strong>v. （芭蕾舞或表演）设计舞蹈动作；设计，筹划（活动、工作）（choreograph 的过去式及过去分词）
adj. 精心安排好的</strong>

<strong>1 : to compose the choreography of

choreograph a ballet</strong>

<strong>2 : to arrange or direct the movements, progress, or details of

a carefully choreographed meeting</strong>

<strong>3 : to engage in choreography</strong>

<i>Synonyms: arrange; blueprint; budget; calculate; chart; design; frame; lay out; map (out); organize; plan; prepare; project; scheme (out); shape; strategize (about); </i>

***

<h3>rattle</h3><strong>vt. 使发出咯咯声；喋喋不休；使慌乱，使惊慌
vi. 喋喋不休地讲话；发出卡嗒卡嗒声
n. 喋喋不休的人；吓吱声，格格声
n. (Rattle)人名；(英)拉特尔</strong>

<strong>1 : to make a rapid succession of short sharp noises

the windows rattled in the wind</strong>

<strong>2 : to chatter incessantly and aimlessly</strong>

<strong>3 : to move with a clatter or rattle</strong>

<strong>4 : to be or move about in a place or station too large or grand</strong>

<i>Synonyms: babel; blare; bluster; bowwow; brawl; bruit; cacophony; chatter; clamor; clangor; decibel(s); din; discordance; katzenjammer; noise; racket; roar; </i>

***

<h3>unrelenting</h3><strong>adj. 无情的；不屈不挠的；不松懈的</strong>

<strong>1 : not softening or yielding in determination : hard, stern
an unrelenting leader</strong>

<strong>2 : not letting up or weakening in vigor or pace : constant
the unrelenting struggle</strong>

<i>Synonyms: adamant; adamantine; bullheaded; dogged; hard; hard-nosed; hardened; hardheaded; headstrong; immovable; implacable; inconvincible; inflexible; intransigent; mulish; obdurate; obstinate; opinionated; ossified; pat; pertinacious; perverse; pigheaded; self-opinionated; self-willed; stiff-necked; stubborn; unbending; uncompromising; unyielding; willful; wilful; </i>

***

<h3>relent</h3><strong>vi. 变温和，变宽厚；减弱；缓和
vt. 使变温和；减轻
v. 再借给（relend的过去分词）</strong>

<strong>1 : to become less severe, harsh, or strict usually from reasons of humanity</strong>

<strong>2 : to cease resistance : give in</strong>

<strong>3 : let up, slacken</strong>

<strong>4 : soften, mollify</strong>

<i>Synonyms: blink; bow; budge; capitulate; concede; give in; knuckle under; quit; submit; succumb; surrender; yield; </i>

***

<h3>panegyrics</h3><strong>n. 颂词，赞颂</strong>

<strong>1 : a eulogistic oration or writing

composed a panegyric about Tchaikovsky for the concert program</strong>

<strong>2 : formal or elaborate praise</strong>

<i>Synonyms: accolade; citation; commendation; dithyramb; encomium; eulogium; eulogy; homage; hymn; paean; salutation; tribute; </i>

***

<h3>lowly</h3><strong>adj. 卑贱的；地位低的；谦逊的</strong>

<strong>1 : in a humble or meek manner</strong>

<strong>2 : in a low position, manner, or degree</strong>

<strong>3 : not loudly</strong>

<i>Synonyms: abjectly; deferentially; hat in hand; humbly; meanly; meekly; modestly; sheepishly; submissively; </i>

***

<h3>calumny</h3><strong>n. 诽谤；中伤；诬蔑</strong>

<strong>1 : a misrepresentation intended to harm another's reputation

denounced his opponent for his defamatory insinuations and calumny</strong>

<strong>2 : the act of uttering false charges or misrepresentations maliciously calculated to harm another's reputation

He was the target of calumny for his unpopular beliefs.</strong>

<i>Synonyms: aspersing; blackening; calumniation; character assassination; defamation; defaming; libel; libeling; libelling; maligning; slander; smearing; traducing; vilification; vilifying; </i>

***

<h3>fulmination</h3><strong>n. 严词谴责；爆发；爆鸣</strong>

<strong>1 : to utter or send out with denunciation

fulminate a decree</strong>

<strong>2 : to send forth censures or invectives

fulminating against government regulators— Mark Singer</strong>

<i>Synonyms: bluster; huff; rant; rave; spout; </i>

***

<h3>fusty</h3><strong>adj. 发霉的；守旧的，老式的</strong>

<strong>1 : impaired by age or dampness : moldy</strong>

<strong>2 : saturated with dust and stale odors : musty</strong>

<strong>3 : rigidly old-fashioned or reactionary</strong>

<i>Synonyms: fetid; foul; frowsty; frowsy; frowzy; funky; malodorous; musty; noisome; rank; reeking; reeky; ripe; smelly; stenchy; stinking; stinky; strong; </i>

***

<h3>forswear</h3><strong>vi. 作伪证
vt. 发誓抛弃</strong>

<strong>1 : to make a liar of (oneself) under or as if under oath</strong>

<strong>2 : to reject or renounce under oath</strong>

<strong>3 : to renounce earnestly</strong>

<strong>4 : to deny under oath</strong>

<strong>5 : to swear falsely</strong>

<i>Synonyms: abjure; abnegate; recant; renege; renounce; repeal; repudiate; retract; take back; unsay; withdraw; </i>

***

<h3>protean</h3><strong>adj. 千变万化的；一人演几个角色的；变形虫的</strong>

<strong>1 : of or resembling Proteus in having a varied nature or ability to assume different forms</strong>

<strong>2 : displaying great diversity or variety : versatile</strong>

<i>Synonyms: adaptable; all-around; all-round; universal; versatile; </i>

***

<h3>girth</h3><strong>n. 周长；围长；肚带
vt. 围绕；包围
vi. 围长为
n. (Girth)人名；(英)格思；(法、德)吉尔特</strong>

<strong>1 : a band or strap that encircles the body of an animal to fasten something (such as a saddle) on its back</strong>

<strong>2 : a measure around a body

a man of more than average girth the girth of a tree</strong>

<strong>3 : size, dimensions</strong>

<i>Synonyms: circumference; </i>

***

<h3>jibe</h3><strong>vt. 嘲笑；嘲弄；<口>符合；相一致；
vi. （=gibe）讥笑﹑ 嘲弄
n. 嘲弄; 嘲讽
v. (=gybe) 顺风时将船帆自一舷转向另一舷以改变方向.</strong>

<strong>1 : to be in accord : agree

—usually used with with a story that doesn't jibe with the facts</strong>

***

<h3>avid</h3><strong>adj. 渴望的，贪婪的；热心的
n. (Avid)人名；(俄)阿维德</strong>

<strong>1 : characterized by enthusiasm and vigorous pursuit : very eager and enthusiastic

avid readers/fans an avid golfer</strong>

<strong>2 : desirous to the point of greed : urgently eager : greedy
avid for publicity/success</strong>

<i>Synonyms: acquisitive; avaricious; coveting; covetous; grabby; grasping; greedy; mercenary; moneygrubbing; rapacious; </i>

***

<h3>purview</h3><strong>n. 范围，权限；视界；条款</strong>

<strong>1 : the body or enacting part of a statute</strong>

<strong>2 : the limit, purpose, or scope of a statute</strong>

<strong>3 : the range or limit of authority, competence, responsibility, concern, or intention</strong>

<strong>4 : range of vision, understanding, or cognizance</strong>

***

<h3>contingency</h3><strong>n. 可能发生的事；偶发事件；不能确定的事件；应急措施；应急储备；应急开支；可能性，偶然性；不测，意外；胜诉酬金
adj. 应变的</strong>

<strong>1 : a contingent event or condition: such as</strong>

<strong>2 : an event (such as an emergency) that may but is not certain to occur

trying to provide for every contingency</strong>

<strong>3 : something liable to happen as an adjunct to or result of something else

the contingencies of war</strong>

<strong>4 : the quality or state of being contingent</strong>

<i>Synonyms: case; contingence; contingent; event; eventuality; possibility; </i>

***

<h3>raillery</h3><strong>n. 逗趣；开玩笑</strong>

<strong>1 : good-natured ridicule : banter</strong>

<strong>2 : jest</strong>

<i>Synonyms: backchat; badinage; banter; chaff; give-and-take; jesting; joshing; persiflage; repartee; </i>

***

<h3>lugubrious</h3><strong>adj. 悲哀的，悲惨的</strong>

<strong>1 : mournful</strong>

<strong>2 : exaggeratedly or affectedly (see affected sense 1) mournful</strong>

<i>Synonyms: black; bleak; cheerless; chill; Cimmerian; cloudy; cold; comfortless; dark; darkening; depressing; depressive; desolate; dire; disconsolate; dismal; drear; dreary; dreich; elegiac; elegiacal; forlorn; funereal; gloomy; glum; godforsaken; gray; grey; lonely; lonesome; miserable; morbid; morose; murky; plutonian; saturnine; sepulchral; solemn; somber; sombre; sullen; sunless; tenebrific; tenebrous; wretched; </i>

***

<h3>madcap</h3><strong>adj. 狂妄的；鲁莽的
n. 鲁莽的人</strong>

<strong>1 : marked by capriciousness, recklessness, or foolishness</strong>

<i>Synonyms: audacious; brash; daredevil; foolhardy; overbold; overconfident; reckless; temerarious; </i>

***

<h3>lurk</h3><strong>vi. 潜伏；潜藏；埋伏
n. 潜伏；埋伏</strong>

<strong>1 : to lie in wait in a place of concealment especially for an evil purpose

someone out there lurking in the shadows</strong>

<strong>2 : to move furtively or inconspicuously

shall I lurk about this country like a thief?— Henry Fielding</strong>

<strong>3 : to persist in staying

the excitement of the first act still lurking in the air— Richard Fletcher Something about the smile lurking on Malfoy's face during the next week made Harry, Ron, and Hermione very nervous.— J. K. Rowling</strong>

<strong>4 : to be concealed but capable of being discovered</strong>

<strong>5 : to constitute a latent threat</strong>

<i>Synonyms: mooch; mouse; pussyfoot; shirk; skulk; slide; slink; slip; snake; sneak; steal; </i>

***

<h3>disposition</h3><strong>n. 处置；[心理] 性情；[军] 部署；倾向</strong>

<strong>1 : prevailing tendency, mood, or inclination</strong>

<strong>2 : temperamental makeup</strong>

<strong>3 : the tendency of something to act in a certain manner under given circumstances</strong>

<strong>4 : the act or the power of disposing or the state of being disposed: such as</strong>

<strong>5 : administration, control</strong>

<strong>6 : final arrangement : settlement
the disposition of the case</strong>

<strong>7 : transfer to the care or possession of another</strong>

<strong>8 : the power of such transferal</strong>

<strong>9 : orderly arrangement</strong>

<i>Synonyms: grain; nature; temper; temperament; </i>

***

<h3>contrail</h3><strong>n. 凝结尾；航迹云；凝迹（飞机飞过留下的尾迹）</strong>

<strong>1 : streaks of condensed water vapor created in the air by an airplane or rocket at high altitudes</strong>

***

<h3>multitudinous</h3><strong>adj. 大量的，众多的；各种各样都有的；由许多不同成分（或人）组成的；浩瀚的，辽阔的</strong>

<strong>1 : including a multitude of individuals : populous
the multitudinous city</strong>

<strong>2 : existing in a great multitude

multitudinous opportunities</strong>

<strong>3 : existing in or consisting of innumerable elements or aspects

multitudinous applause</strong>

<i>Synonyms: beaucoup; legion; many; multifold; multiple; multiplex; numerous; </i>

***

<h3>obtuse</h3><strong>adj. 迟钝的；圆头的；不锋利的</strong>

<strong>1 : not pointed or acute : blunt</strong>

<strong>2 : exceeding 90 degrees but less than 180 degrees</strong>

<strong>3 : having an obtuse angle

an obtuse triangle
— see triangle illustration</strong>

<strong>4 — see triangle illustration</strong>

<strong>5 : rounded at the free end</strong>

<strong>6 : lacking sharpness or quickness of sensibility or intellect : insensitive, stupid
He is too obtuse to take a hint.</strong>

<strong>7 : difficult to comprehend : not clear or precise in thought or expression

It is also, unfortunately, ill-written, and at times obtuse and often trivial.— Shirley Hazzard</strong>

<i>Synonyms: blunt; blunted; dull; dulled; </i>

***

<h3>vociferous</h3><strong>adj. 大声叫的，喊叫的，喧嚷的；激昂的</strong>

<strong>1 : marked by or given to vehement insistent outcry</strong>

<i>Synonyms: blatant; caterwauling; clamant; clamorous; obstreperous; squawking; vociferant; vociferating; yawping; yauping; yowling; </i>

***

<h3>cajole</h3><strong>vt. 以甜言蜜语哄骗；勾引</strong>

<strong>1 : to persuade with flattery or gentle urging especially in the face of reluctance : coax
had to cajole them into going</strong>

<strong>2 : to obtain from someone by gentle persuasion

cajoled money from his parents</strong>

<strong>3 : to deceive with soothing words or false promises

cajoled himself with thoughts of escape— Robertson Davies</strong>

<i>Synonyms: blandish; blarney; coax; palaver; soft-soap; sweet-talk; wheedle; </i>

***

<h3>coax</h3><strong>vt. 哄；哄诱；慢慢将…弄好
vi. 哄骗；劝诱</strong>

<strong>1 : to influence or gently urge by caressing or flattering : wheedle
coaxed him into going</strong>

<strong>2 : to draw, gain, or persuade by means of gentle urging or flattery

unable to coax an answer out of him coaxing consumers to buy new cars</strong>

<strong>3 : to manipulate with great perseverance and usually with considerable effort toward a desired state or activity

coax a fire to burn is optimistic that stem cells can be coaxed into growing into replacement tissue for failing organs</strong>

<strong>4 : fondle, pet</strong>

<i>Synonyms: blandish; blarney; cajole; palaver; soft-soap; sweet-talk; wheedle; </i>

***

<h3>prophylactic</h3><strong>adj. 预防性的，预防疾病的
n. 预防性药物（或器具、措施）；预防法；避孕用具</strong>

<strong>1 : guarding from or preventing the spread or occurrence of disease or infection</strong>

<strong>2 : tending to prevent or ward off : preventive</strong>

<i>Synonyms: precautionary; preventative; preventive; </i>

***

<h3>extirpated</h3><strong>v. 彻底消灭，根除（extirpate 的过去式和过去分词）</strong>

<strong>1 : to destroy completely : wipe out</strong>

<strong>2 : to pull up by the root</strong>

<strong>3 : to cut out by surgery</strong>

<i>Synonyms: abolish; annihilate; black out; blot out; cancel; clean (up); efface; eradicate; erase; expunge; exterminate; liquidate; obliterate; root (out); rub out; snuff (out); stamp (out); sweep (away); wipe out; </i>

***

<h3>inclusiveness</h3><strong>n. 包容；包容性</strong>

<strong>1 : comprehending stated limits or extremes

from Monday to Friday inclusive</strong>

<strong>2 : broad in orientation or scope</strong>

<strong>3 : covering or intended to cover all items, costs, or services</strong>

<i>Synonyms: all-embracing; all-in; all-inclusive; broad-gauge; broad-gauged; compendious; complete; comprehensive; cover-all; cyclopedic; embracive; encyclopedic; exhaustive; full; global; in-depth; omnibus; panoramic; thorough; universal; </i>

***

<h3>emancipate</h3><strong>vt. 解放；释放</strong>

<strong>1 : to free from restraint, control, or the power of another</strong>

<strong>2 : to free from bondage</strong>

<strong>3 : to release from parental care and responsibility and make sui juris</strong>

<strong>4 : to free from any controlling influence (such as traditional mores or beliefs)</strong>

<i>Synonyms: discharge; disenthrall; disenthral; enfranchise; enlarge; free; liberate; loose; loosen; manumit; release; spring; unbind; uncage; unchain; unfetter; </i>

***

<h3>factuality</h3><strong>n. 实在性；事实性</strong>

<strong>1 : of or relating to facts
a factual error the factual aspects of the case</strong>

<strong>2 : restricted to or based on fact

a factual statement She tried to separate what is factual from what is not.</strong>

<i>Synonyms: documentary; hard; historical; literal; matter-of-fact; nonfictional; objective; true; </i>

***

<h3>obscurity</h3><strong>n. 朦胧；阴暗；晦涩；身份低微；不分明</strong>

<strong>1 : one that is obscure
… peppered with quotes from … heavy hitters, as well as some downright obscurities.— Penelope Green</strong>

<strong>2 : the quality or state of being obscure

novels that have faded into obscurity</strong>

<i>Synonyms: ambiguity; ambiguousness; darkness; equivocalness; equivocation; inscrutability; inscrutableness; murkiness; mysteriousness; nebulosity; nebulousness; obliqueness; obliquity; opacity; opaqueness; </i>

***

<h3>fanatical</h3><strong>adj. 狂热的</strong>

<strong>1 : a person exhibiting excessive enthusiasm and intense uncritical devotion toward some controversial matter (as in religion or politics)

a religious fanatic [=extremist] The fanatics are convinced they are serving a righteous cause and that all means are justified …— Flora Lewis</strong>

<strong>2 : a person who is extremely enthusiastic about and devoted to some interest or activity

a boating/sports/racing fanatic She's a real fanatic when it comes to working out. Since the U.S. economy began to sputter in 2008, shoppers have become coupon fanatics and lovers of buy-one-get-one-free deals …— Janet K. Keeler</strong>

<i>Synonyms: addict; aficionado; afficionado; buff; bug; devotee; enthusiast; fan; fancier; fiend; fool; freak; habitué; habitue; head; hound; junkie; junky; lover; maniac; maven; mavin; nut; sucker; </i>

***

<h3>archetypal</h3><strong>adj. [生物] 原型的</strong>

<strong>1 : the original pattern or model of which all things of the same type are representations or copies : prototype
… the House of Commons, the archetype of all the representative assemblies which now meet …— Thomas Babington Macaulay


also
: a perfect example 
He is the archetype of a successful businessman.</strong>

<strong>2 : a perfect example</strong>

<strong>3 : idea sense 4c</strong>

<strong>4 : an inherited idea or mode of thought in the psychology of C. G. Jung that is derived from the experience of the race and is present in the unconscious of the individual</strong>

<i>Synonyms: ancestor; antecedent; daddy; foregoer; forerunner; granddaddy; grandaddy; precursor; predecessor; prototype; </i>

***

<h3>obstinate</h3><strong>adj. 顽固的；倔强的；难以控制的</strong>

<strong>1 : stubbornly adhering to an opinion, purpose, or course in spite of reason, arguments, or persuasion

obstinate resistance to change</strong>

<strong>2 : not easily subdued, remedied, or removed

obstinate fever</strong>

<i>Synonyms: adamant; adamantine; bullheaded; dogged; hard; hardened; hardheaded; hard-nosed; headstrong; immovable; implacable; inconvincible; inflexible; intransigent; mulish; obdurate; opinionated; ossified; pat; pertinacious; perverse; pigheaded; self-opinionated; self-willed; stiff-necked; stubborn; unbending; uncompromising; unrelenting; unyielding; willful; wilful; </i>

***

<h3>staunch</h3><strong>adj. 坚定的；忠诚的；坚固的
vt. 止住；止血
n. (Staunch)人名；(英)斯汤奇</strong>

<strong>1 : steadfast in loyalty or principle

a staunch friend</strong>

<strong>2 : watertight, sound</strong>

<strong>3 : strongly built : substantial</strong>

<i>Synonyms: constant; dedicated; devoted; devout; down-the-line; faithful; fast; good; loyal; pious; steadfast; steady; true; true-blue; </i>

***

<h3>fealty</h3><strong>n. 忠诚；忠实</strong>

<strong>1 : the fidelity of a vassal or feudal tenant to his lord</strong>

<strong>2 : the obligation of such fidelity

The vassal vowed fealty to the king.</strong>

<strong>3 : intense fidelity

the fealty of country music fans to their favorite stars— Nicholas Dawidoff</strong>

<i>Synonyms: adhesion; allegiance; attachment; commitment; constancy; dedication; devotedness; devotion; faith; faithfulness; fastness; fidelity; loyalty; piety; steadfastness; troth; </i>

***

<h3>compassionate</h3><strong>adj. 慈悲的；富于同情心的
vt. 同情；怜悯</strong>

<strong>1 : having or showing compassion : sympathetic
a compassionate friend a compassionate smile</strong>

<strong>2 : granted because of unusual distressing circumstances affecting an individual

—used of some military privileges (such as leave) The soldier was granted compassionate leave following the death of his father.</strong>

<i>Synonyms: beneficent; benevolent; benignant; good-hearted; humane; kind; kindhearted; kindly; softhearted; sympathetic; tender; tenderhearted; warmhearted; </i>

***

<h3>devious</h3><strong>adj. 偏僻的；弯曲的；不光明正大的</strong>

<strong>1 : wandering, roundabout
a devious path</strong>

<strong>2 : moving without a fixed course : errant
devious breezes</strong>

<strong>3 : out-of-the-way, remote
upon devious coasts</strong>

<strong>4 : deviating from a right, accepted, or common course

devious conduct</strong>

<strong>5 : not straightforward : cunning
a devious politician</strong>

<strong>6 : deceptive</strong>

<i>Synonyms: artful; beguiling; cagey; cagy; crafty; cunning; cute; designing; dodgy; foxy; guileful; scheming; shrewd; slick; sly; subtle; tricky; wily; </i>

***

<h3>invective</h3><strong>n. 恶言漫骂，咒骂；猛烈抨击
adj. 谩骂的；恶言的</strong>

<strong>1 : insulting or abusive language : vituperation</strong>

<strong>2 : an abusive expression or speech</strong>

<i>Synonyms: abuse; billingsgate; fulmination; obloquy; scurrility; vitriol; vituperation; </i>

***

<h3>reassure</h3><strong>vt. 使安心；使消除疑虑；安慰；重新保证，再次确保；（尤指保险）分保，给……再保险</strong>

<strong>1 : to assure anew

reassured him that the work was on schedule</strong>

<strong>2 : to restore to confidence

felt reassured by their earnest promise to do better</strong>

<strong>3 : reinsure</strong>

<i>Synonyms: assure; cheer; comfort; console; solace; soothe; </i>

***

<h3>reprehend</h3><strong>vt. 申斥；指责
n. 责备</strong>

<strong>1 : to voice disapproval of : censure</strong>

<i>Synonyms: anathematize; censure; condemn; damn; decry; denounce; execrate; reprobate; </i>

***

<h3>exonerate</h3><strong>vt. 使免罪</strong>

<strong>1 : to relieve of a responsibility, obligation, or hardship</strong>

<strong>2 : to clear from accusation or blame</strong>

<i>Synonyms: absolve; acquit; clear; exculpate; vindicate; </i>

***

<h3>strident</h3><strong>adj. 刺耳的；尖锐的；吱吱尖叫的；轧轧作响的</strong>

<strong>1 : characterized by harsh, insistent, and discordant sound

a strident voice</strong>

<strong>2 : commanding attention by a loud or obtrusive quality</strong>

***

<h3>strenuous</h3><strong>adj. 紧张的；费力的；奋发的；艰苦的；热烈的</strong>

<strong>1 : vigorously active : energetic</strong>

<strong>2 : fervent, zealous
his most strenuous supporters</strong>

<strong>3 : marked by or calling for energy or stamina : arduous
a strenuous hike</strong>

<i>Synonyms: aggressive; assertive; dynamic; emphatic; energetic; forceful; full-blooded; muscular; resounding; vehement; vigorous; violent; </i>

***

<h3>irresistible</h3><strong>adj. 极度诱人的；不可抵抗的</strong>

<strong>1 : impossible to resist

an irresistible attraction</strong>

***

<h3>solace</h3><strong>n. 安慰；慰藉；安慰之物
vt. 安慰；抚慰；使快乐（过去式solaced，过去分词solaced，现在分词solacing，第三人称单数solaces）</strong>

<strong>1 : to give comfort to in grief or misfortune : console</strong>

<strong>2 : to make cheerful</strong>

<strong>3 : amuse</strong>

<strong>4 : allay, soothe
solace grief</strong>

<i>Synonyms: assure; cheer; comfort; console; reassure; soothe; </i>

***

<h3>liable</h3><strong>adj. 有责任的，有义务的；应受罚的；有…倾向的；易…的</strong>

<strong>1 : obligated according to law or equity (see equity sense 3) : responsible
liable for the debts incurred by his wife</strong>

<strong>2 : subject to appropriation or attachment

All his property is liable to pay his debts.</strong>

<strong>3 : being in a position to incur

—used with to liable to a fine</strong>

<strong>4 : exposed or subject to some usually adverse contingency or action

watch out or you're liable to fall</strong>

<i>Synonyms: endangered; exposed; open; sensitive; subject (to); susceptible; vulnerable; </i>

***

<h3>abet</h3><strong>vt. 煽动，教唆；支持
n. (Abet)人名；(意)阿贝特；(匈)奥拜特</strong>

<strong>1 : to actively second and encourage (something, such as an activity or plan)

abet the commission of a crime</strong>

<strong>2 : to assist or support (someone) in the achievement of a purpose

The singer was abetted by a skillful accompanist.</strong>

<strong>3 : to assist, encourage, instigate, or support with criminal intent in attempting or carrying out a crime</strong>

<i>Synonyms: brew; ferment; foment; incite; instigate; pick; provoke; raise; stir (up); whip (up); </i>

***

<h3>wondrous</h3><strong>adj. 奇妙的；令人惊奇的；非常的</strong>

<strong>1 : that is to be marveled at : extraordinary
a wondrous feat</strong>

<i>Synonyms: amazing; astonishing; astounding; awesome; awful; eye-opening; fabulous; marvelous; marvellous; miraculous; portentous; prodigious; staggering; stunning; stupendous; sublime; surprising; wonderful; </i>

***

<h3>erratic</h3><strong>adj. 不稳定的；古怪的
n. 漂泊无定的人；古怪的人</strong>

<strong>1 : having no fixed course : wandering
an erratic comet</strong>

<strong>2 : characterized by lack of consistency, regularity, or uniformity

erratic dieting keeps erratic hours</strong>

<strong>3 : deviating from what is ordinary or standard : eccentric
an erratic genius</strong>

<strong>4 : transported from an original resting place especially by a glacier
an erratic boulder</strong>

<strong>5 : nomadic</strong>

<i>Synonyms: aimless; arbitrary; catch-as-catch-can; desultory; haphazard; helter-skelter; hit-or-miss; random; scattered; slapdash; stray; </i>

***

<h3>outlandish</h3><strong>adj. 古怪的；奇异的；异国风格的；偏僻的</strong>

<strong>1 : of or relating to another country : foreign
saw many outlandish animals at the zoo</strong>

<strong>2 : strikingly out of the ordinary : bizarre
an outlandish costume Her book is filled with outlandish characters. spun some outlandish tales</strong>

<strong>3 : exceeding proper or reasonable limits or standards

workers complain of outlandish hours— Joan E. Rigdon outlandish government specifications</strong>

<strong>4 : remote from civilization

no other young men foolish enough to offer to go to such an outlandish station — Geog. Jour.</strong>

<i>Synonyms: bizarre; bizarro; cranky; crazy; curious; eccentric; erratic; far-out; funky; funny; kinky; kooky; kookie; odd; off-kilter; off-the-wall; offbeat; out-of-the-way; outré; peculiar; quaint; queer; queerish; quirky; remarkable; rum; screwy; spaced-out; strange; wacky; whacky; way-out; weird; weirdo; wild; </i>

***

<h3>consternation</h3><strong>n. 惊愕；惊惶失措；恐怖</strong>

<strong>1 : amazement or dismay that hinders or throws into confusion

the two … stared at each other in consternation, and neither knew what to do— Pearl Buck</strong>

***

<h3>equanimity</h3><strong>n. 平静；镇定</strong>

<strong>1 : evenness of mind especially under stress

nothing could disturb his equanimity</strong>

<strong>2 : right disposition : balance
physical equanimity</strong>

<i>Synonyms: aplomb; calmness; collectedness; composedness; composure; cool; coolness; countenance; equilibrium; imperturbability; placidity; repose; sangfroid; self-composedness; self-possession; serenity; tranquillity; tranquility; tranquilness; </i>

***

<h3>disquieting</h3><strong>adj. 令人不安的；令人忧虑的
v. 使不安；使忧虑（disquiet的ing形式）</strong>

<strong>1 : to take away the peace or tranquility of : disturb, alarm
were disquieted by recent events</strong>

<i>Synonyms: agitate; ail; alarm; alarum; bother; concern; derail; discomfort; discompose; dismay; distemper; distract; distress; disturb; exercise; flurry; frazzle; freak (out); fuss; hagride; perturb; undo; unhinge; unsettle; upset; weird out; worry; </i>

***

<h3>credulity</h3><strong>n. 轻信；易受骗</strong>

<strong>1 : readiness or willingness to believe especially on slight or uncertain evidence

Her description of the event strains credulity.</strong>

<i>Synonyms: credulousness; gullibility; naiveness; naïveté; naivete; simpleness; </i>

***

<h3>forsaken</h3><strong>adj. 被抛弃的，孤独凄凉的
v. 抛弃，遗弃；拒绝，放弃（forsake 的过去分词）</strong>

<strong>1 : to renounce or turn away from entirely

friends have forsaken her forsook the theater for politics</strong>

<i>Synonyms: abandon; desert; leave; maroon; quit; strand; </i>

***

<h3>eulogized</h3><strong>vt. 颂扬；称赞</strong>

<strong>1 : to speak or write in high praise of : extol</strong>

***

<h3>enviable</h3><strong>adj. 值得羡慕的；引起忌妒的</strong>

<strong>1 : highly desirable</strong>

***

<h3>surmise</h3><strong>vt. 猜测；推测
vi. 猜测；认为
n. 推测；猜度</strong>

<strong>1 : a thought or idea based on scanty evidence : conjecture</strong>

<i>Synonyms: conjecture; guess; shot; supposition; </i>

***

<h3>gourmet</h3><strong>n. 美食家
菜肴精美的</strong>

<strong>1 : a connoisseur of food and drink</strong>

<strong>2 : connoisseur sense 2</strong>

<i>Synonyms: bon vivant; epicure; epicurean; gastronome; gastronomist; gourmand; </i>

***

<h3>stiff</h3><strong>adj. 呆板的；坚硬的；僵硬的；严厉的；拘谨的；稠的；（价格）高昂的；（酒）烈性的
adv. 极其；僵硬地；彻底地
n. 死尸；令人讨厌者；流通票据；劳动者
vt. 诈骗；失信</strong>

<strong>1 : not easily bent : rigid
a stiff collar</strong>

<strong>2 : lacking in suppleness or flexibility

stiff muscles</strong>

<strong>3 : impeded in movement

—used of a mechanism a truck's stiff suspension</strong>

<strong>4 : drunk sense 1a</strong>

<strong>5 : firm, resolute</strong>

<strong>6 : stubborn, unyielding</strong>

<strong>7 : proud</strong>

<strong>8 : marked by reserve or decorum</strong>

<strong>9 : lacking in ease or grace : stilted</strong>

<strong>10 : hard fought

stiff competition</strong>

<strong>11 : exerting great force

a stiff wind</strong>

<strong>12 : forceful, vigorous</strong>

<strong>13 : potent
poured her a stiff drink</strong>

<strong>14 : of a dense or glutinous consistency : thick</strong>

<strong>15 : harsh, severe
a stiff penalty</strong>

<strong>16 : arduous, rugged
stiff terrain</strong>

<strong>17 : not easily heeled over by an external force (such as the wind)

a stiff ship</strong>

<strong>18 : expensive, steep
paid a stiff price</strong>

<i>Synonyms: inflexible; rigid; stiffened; unyielding; </i>

***

<h3>contravene</h3><strong>vt. 抵触；违反；反驳；否认</strong>

<strong>1 : to go or act contrary to : violate
contravene a law</strong>

<strong>2 : to oppose in argument : contradict
contravene a proposition</strong>

<i>Synonyms: breach; break; fracture; infringe (on or upon); offend; traduce; transgress; violate; </i>

***

<h3>dismantle</h3><strong>vt. 拆除；取消；解散；除掉…的覆盖物
vi. 可拆卸</strong>

<strong>1 : to disconnect the pieces of

will have to dismantle the engine</strong>

<strong>2 : to destroy the integrity or functioning of</strong>

<strong>3 : to strip of dress or covering : divest</strong>

<strong>4 : to strip of furniture and equipment

dismantled the ship before scrapping it</strong>

<i>Synonyms: break down; demount; disassemble; dismember; dismount; knock down; strike; take down; </i>

***

<h3>testament</h3><strong>n. [法] 遗嘱；圣约；确实的证明</strong>

<strong>1 : a tangible proof or tribute</strong>

<strong>2 : an expression of conviction : creed</strong>

<strong>3 : an act by which a person determines the disposition of his or her property after death</strong>

<strong>4 : will</strong>

<strong>5 : either of two main divisions of the Bible</strong>

<strong>6 : a covenant between God and the human race</strong>

<i>Synonyms: attestation; confirmation; corroboration; documentation; evidence; proof; substantiation; testimonial; testimony; validation; voucher; witness; </i>

***

<h3>turbid</h3><strong>adj. 浑浊的；混乱的；雾重的</strong>

<strong>1 : thick or opaque with or as if with roiled sediment

a turbid stream</strong>

<strong>2 : heavy with smoke or mist</strong>

<strong>3 : deficient in clarity or purity : foul, muddy
turbid depths of degradation and misery— C. I. Glicksberg</strong>

<strong>4 : characterized by or producing obscurity (as of mind or emotions)

an emotionally turbid response</strong>

<i>Synonyms: cloudy; muddy; riley; roiled; </i>

***

<h3>brackish</h3><strong>adj. 含盐的；令人不快的；难吃的</strong>

<strong>1 : somewhat salty

brackish water</strong>

<strong>2 : not appealing to the taste

brackish tea</strong>

<strong>3 : repulsive
a brackish personality</strong>

<i>Synonyms: distasteful; unappetizing; unpalatable; unsavory; yucky; yukky; </i>

***

<h3>fathomless</h3><strong>adj. 深不可测的；不可了解的</strong>

<strong>1 : incapable of being fathomed : immeasurable
fathomless powers of gravity and chemistry— R. W. Emerson</strong>

<i>Synonyms: bottomless; boundless; endless; horizonless; illimitable; immeasurable; immensurable; indefinite; infinite; limitless; measureless; unbounded; unfathomable; unlimited; </i>

***

<h3>quell</h3><strong>vt. 平息；镇压；减轻；消除
n. (Quell)人名；(捷)奎尔；(西)克利</strong>

<strong>1 : to thoroughly overwhelm and reduce to submission or passivity

quell a riot</strong>

<strong>2 : quiet, pacify
quell fears</strong>

<i>Synonyms: clamp down (on); crack down (on); crush; put down; quash; repress; silence; slap down; snuff (out); squash; squelch; subdue; suppress; </i>

***

<h3>lingering</h3><strong>adj. 拖延的，迟迟不结束的
v. 逗留；磨蹭，拖延；继续存留，缓慢消失；持续看或思考；苟延残喘（linger 的现在分词）</strong>

<strong>1 : to be slow in parting or in quitting something : tarry
fans lingered outside the door</strong>

<strong>2 : to remain existent although often waning in strength, importance, or influence

lingering doubts lingering odors</strong>

<strong>3 : to remain alive although gradually dying

was seriously ill, but lingered on for several months</strong>

<strong>4 : to be slow to act : procrastinate
He lingered in settling the estate in order to increase his fees.</strong>

<strong>5 : to move slowly : saunter
lingering homeward</strong>

<strong>6 : to pass (a period of time) slowly</strong>

<strong>7 : delay</strong>

<i>Synonyms: crawl; creep; dally; dawdle; delay; diddle; dillydally; drag; lag; loiter; lollygag; lallygag; mope; poke; shilly-shally; tarry; </i>

***

<h3>illicit</h3><strong>adj. 违法的；不正当的</strong>

<strong>1 : not permitted : unlawful</strong>

<i>Synonyms: criminal; felonious; illegal; illegitimate; lawless; unlawful; wrongful; </i>

***

<h3>apprehension</h3><strong>n. 理解；恐惧；逮捕；忧惧</strong>

<strong>1 : suspicion or fear especially of future evil : foreboding
an atmosphere of nervous apprehension</strong>

<strong>2 : seizure by legal process : arrest
apprehension of a criminal</strong>

<strong>3 : the act or power of perceiving or comprehending something

a person of dull apprehension</strong>

<strong>4 : the result of apprehending something mentally : conception
according to popular apprehension</strong>

<i>Synonyms: alarm; alarum; apprehensiveness; dread; foreboding; misgiving; </i>

***

<h3>maverick</h3><strong>n. 没打烙印的动物；持不同意见的人
vt. 用不正当手段获取
vi. 迷途；背离
adj. 未打烙印的；行为不合常规的；特立独行的</strong>

<strong>1 : an unbranded range animal</strong>

<strong>2 : a motherless calf</strong>

<strong>3 : an independent individual who does not go along with a group or party</strong>

<i>Synonyms: bohemian; boho; counterculturist; deviant; enfant terrible; free spirit; heretic; iconoclast; individualist; lone ranger; lone wolf; loner; nonconformer; nonconformist; </i>

***

<h3>malefactor</h3><strong>n. 作恶者；罪犯；坏人</strong>

<strong>1 : one who commits an offense against the law</strong>

<strong>2 : felon</strong>

<i>Synonyms: evildoer; immoralist; sinner; wrongdoer; </i>

***

<h3>paragon</h3><strong>n. 模范；完美之物；优秀之人
adj. 完美的
n. (Paragon)人名；(法)帕拉贡</strong>

<strong>1 : a model of excellence or perfection

was a paragon of goodness a paragon of a wife</strong>

<i>Synonyms: beau ideal; classic; eidolon; exemplar; idea; ideal; model; nonesuch; nonpareil; patron saint; </i>

***

<h3>instigation</h3><strong>n. 煽动，鼓动；教唆；刺激</strong>

<strong>1 : to goad or urge forward : provoke</strong>

<i>Synonyms: abet; brew; ferment; foment; incite; pick; provoke; raise; stir (up); whip (up); </i>

***

<h3>leniency</h3><strong>n. 宽大，仁慈；温和</strong>

<strong>1 : the quality or state of being lenient
the leniency of the punishment</strong>

<strong>2 : a lenient disposition or practice

The prisoner asked the judge for leniency.</strong>

<i>Synonyms: charity; clemency; forbearance; lenience; lenity; mercifulness; mercy; quarter; </i>

***

<h3>oblivious</h3><strong>adj. 遗忘的；健忘的；不注意的；不知道的</strong>

<strong>1 : lacking remembrance, memory, or mindful attention</strong>

<strong>2 : lacking active conscious knowledge or awareness

—usually used with of or to</strong>

<i>Synonyms: clueless; ignorant; incognizant; innocent; insensible; nescient; unacquainted; unaware; unconscious; uninformed; unknowing; unmindful; unwitting; </i>

***

<h3>formidable</h3><strong>adj. 强大的；可怕的；令人敬畏的；艰难的</strong>

<strong>1 : causing fear, dread, or apprehension
a formidable prospect</strong>

<strong>2 : having qualities that discourage approach or attack

a formidable opponent</strong>

<strong>3 : tending to inspire awe or wonder : impressive
a formidable accomplishment</strong>

<i>Synonyms: alarming; dire; direful; dread; dreadful; fearful; fearsome; forbidding; frightening; frightful; ghastly; hair-raising; horrendous; horrible; horrifying; intimidating; redoubtable; scary; shocking; spine-chilling; terrible; terrifying; </i>

***

<h3>stalemate</h3><strong>n. 僵局；陷于困境
vt. 使僵持；使陷入困境
vi. 僵持；陷入僵局</strong>

<strong>1 : a drawing position in chess in which a player is not in checkmate but has no legal move to play</strong>

<strong>2 : a drawn contest : deadlock</strong>

<strong>3 : the state of being stalemated</strong>

<i>Synonyms: deadlock; gridlock; halt; impasse; logjam; Mexican standoff; standoff; standstill; </i>

***

<h3>heft</h3><strong>n. 重量；重要性
vt. 举起；举起试重量
vi. 称重量
n. (Heft)人名；(英)赫夫特</strong>

<strong>1 : weight, heaviness</strong>

<strong>2 : importance, influence</strong>

<strong>3 : the greater part of something : bulk</strong>

<i>Synonyms: avoirdupois; heaviness; weight; </i>

***

<h3>heedless</h3><strong>adj. 不注意的；不留心的</strong>

<strong>1 : not taking heed : inconsiderate, thoughtless
heedless follies of unbridled youth— John DeBruyn</strong>

<i>Synonyms: careless; incautious; mindless; unguarded; unsafe; unwary; </i>

***

<h3>untrammelled</h3><strong>adj. 不受限制的；自由自在的（等于untrammeled）</strong>

<strong>1 : not confined, limited, or impeded

untrammeled greed/arrogance the untrammeled free market Since the later 1990s, when the first news sites were introduced on the Internet, most papers have offered untrammeled access to them.— Michael Massing She looks back with amazement at the untrammeled single life she enjoyed not so long ago.— Paul Gray</strong>

***

<h3>exiguity</h3><strong>n. 微小；些许，稀少</strong>

<strong>1 : the quality or state of being exiguous : scantiness</strong>

***

<h3>hasty</h3><strong>adj. 轻率的；匆忙的；草率的；懈怠的
n. (Hasty)人名；(英)黑斯蒂
[ 比较级 hastier 最高级 hastiest ]</strong>

<strong>1 : done or made in a hurry

hasty city-street snapshots— R. B. Heilman</strong>

<strong>2 : fast and typically superficial

made a hasty examination of the wound</strong>

<strong>3 : rapid in action or movement : speedy</strong>

<strong>4 : acting too quickly : overly eager or impatient

realized he had been hasty in quitting his job</strong>

<strong>5 : exhibiting a lack of careful thought or consideration : precipitate, rash
We don't want to make any hasty decisions.</strong>

<strong>6 : prone to anger : irritable
a hasty temperment</strong>

<i>Synonyms: cursory; drive-by; flying; gadarene; headlong; helter-skelter; hurried; overhasty; pell-mell; precipitate; precipitous; rash; rushed; </i>

***

<h3>flaccid</h3><strong>adj. 弛缓的，软弱的；蔫软的；无活力的；不见成效的</strong>

<strong>1 : not firm or stiff</strong>

<strong>2 : lacking normal or youthful firmness</strong>

<i>Synonyms: droopy; floppy; lank; limp; yielding; </i>

***

<h3>reparation</h3><strong>n. 赔偿；修理；赔款</strong>

<strong>1 : a repairing or keeping in repair</strong>

<strong>2 : repairs</strong>

<strong>3 : the act of making amends, offering expiation, or giving satisfaction for a wrong or injury</strong>

<strong>4 : something done or given as amends or satisfaction</strong>

<strong>5 : the payment of damages : indemnification</strong>

<strong>6 : compensation in money or materials payable by a defeated nation for damages to or expenditures sustained by another nation as a result of hostilities with the defeated nation</strong>

<i>Synonyms: compensation; damages; indemnification; indemnity; quittance; recompense; recoupment; redress; remuneration; reprisal(s); requital; restitution; satisfaction; </i>

***

<h3>denunciation</h3><strong>n. 谴责，斥责；告发</strong>

<strong>1 : an act of denouncing
the denunciation of violence the denunciation of one's enemies their denunciation of him as a spy</strong>

<strong>2 : a public condemnation</strong>

<i>Synonyms: censure; commination; condemnation; excoriation; objurgation; rebuke; reprimand; reproach; reproof; riot act; stricture; </i>

***

<h3>pernicious</h3><strong>adj. 有害的；恶性的；致命的；险恶的</strong>

<strong>1 : highly injurious or destructive : deadly</strong>

<strong>2 : wicked</strong>

<i>Synonyms: adverse; bad; baleful; baneful; damaging; dangerous; deleterious; detrimental; evil; harmful; hurtful; ill; injurious; mischievous; nocuous; noxious; prejudicial; wicked; </i>

***

<h3>outmaneuver</h3><strong>vt. 以计谋胜过；运用策略击败</strong>

<strong>1 : to outdo, defeat, or gain an advantage over by skillful or clever maneuvering : to maneuver more effectively than

Battalions are especially important in desert warfare because they are powerful enough to deliver devastating blows … but small enough to sneak along ridges and valleys to outmaneuver larger forces.— Bob Davis … an amorally ambitious studio exec who disposes of a threatening screenwriter as coolly as he outmaneuvers a rival executive.— Gregg Kilday Sabre pilots learned that if they could force their adversaries to change direction rapidly, the Sabre could outmaneuver them.— Fred Reed</strong>

<i>Synonyms: fox; outfox; outslick; outsmart; outthink; outwit; overreach; </i>

***

<h3>coerce</h3><strong>vt. 强制，迫使</strong>

<strong>1 : to compel to an act or choice

was coerced into agreeing abusers who coerce their victims into silence</strong>

<strong>2 : to achieve by force or threat

coerce compliance coerce obedience</strong>

<strong>3 : to restrain or dominate by force

religion in the past has tried to coerce the irreligious— W. R. Inge</strong>

<i>Synonyms: blackjack; compel; constrain; dragoon; drive; force; impel; impress; make; muscle; obligate; oblige; press; pressure; sandbag; </i>

***

<h3>expedition</h3><strong>n. 远征；探险队；迅速</strong>

<strong>1 : a journey or excursion undertaken for a specific purpose</strong>

<strong>2 : the group of persons making such a journey</strong>

<strong>3 : efficient promptness : speed</strong>

<strong>4 : a sending or setting forth</strong>

<i>Synonyms: journey; passage; peregrination; travel(s); trek; trip; </i>

***

<h3>cranky</h3><strong>adj. 暴躁的；古怪的；动摇的</strong>

<strong>1 : given to fretful fussiness : readily angered when opposed : crotchety
our long wait … left us a tad cranky — Connecticut</strong>

<strong>2 : marked by eccentricity

his bizarre and cranky mix of ideas and beliefs— R. J. Evans</strong>

<strong>3 : full of twists and turns : tortuous
a cranky road</strong>

<strong>4 : working erratically : unpredictable
a cranky old tractor</strong>

<strong>5 : crazy, silly</strong>

***

<h3>utterance</h3><strong>n. 表达；说话；说话方式</strong>

<strong>1 : the last extremity : bitter end</strong>

***

<h3>colloquial</h3><strong>adj. 白话的；通俗的；口语体的</strong>

<strong>1 : used in or characteristic of familiar and informal conversation

In colloquial English, "kind of" is often used for "somewhat" or "rather."</strong>

<strong>2 : unacceptably informal</strong>

<strong>3 : using conversational style

a colloquial writer</strong>

<strong>4 : of or relating to conversation : conversational
colloquial expressions</strong>

<i>Synonyms: conversational; informal; nonformal; nonliterary; unbookish; unliterary; vernacular; vulgar; </i>

***

<h3>insurrection</h3><strong>n. 暴动；叛乱</strong>

<strong>1 : an act or instance of revolting against civil authority or an established government</strong>

<i>Synonyms: insurgence; insurgency; mutiny; outbreak; rebellion; revolt; revolution; rising; uprising; </i>

***

<h3>objectionable</h3><strong>adj. 讨厌的；会引起反对的；有异议的</strong>

<strong>1 : undesirable, offensive</strong>

<i>Synonyms: censurable; exceptionable; obnoxious; offensive; reprehensible; </i>

***

<h3>cronyism</h3><strong>n. 任人唯亲；任用亲信</strong>

<strong>1 : partiality to cronies especially as evidenced in the appointment of political hangers-on to office without regard to their qualifications</strong>

***

<h3>cavilling</h3><strong>卡维尔（男子名）</strong>

<strong>1 : to raise trivial and frivolous objection

The author caviled about the design of the book's cover.</strong>

<strong>2 : to raise trivial objections to

He caviled the conditions of the agreement.</strong>

<i>Synonyms: carp; fuss; niggle; nitpick; quibble; </i>

***

<h3>appropriation</h3><strong>n. 拨款；挪用</strong>

<strong>1 : an act or instance of appropriating something</strong>

<strong>2 : something that has been appropriated</strong>

<strong>3 : money set aside by formal action for a specific use</strong>

<i>Synonyms: allocation; allotment; annuity; entitlement; grant; subsidy; subvention; </i>

***

<h3>unnerved</h3><strong>adj. 气馁的；烦恼不安的
v. 使失去气力；使焦躁（unnerve的过去分词形式）</strong>

<strong>1 : to deprive of courage, strength, or steadiness</strong>

<strong>2 : to cause to become nervous : upset</strong>

<i>Synonyms: demoralize; emasculate; paralyze; undo; unman; unstring; </i>

***

<h3>entanglement</h3><strong>n. 纠缠；铁丝网；缠绕物；牵连</strong>

<strong>1 : the action of entangling : the state of being entangled</strong>

<strong>2 : something that entangles, confuses, or ensnares

a project delayed by legal entanglements</strong>

<strong>3 : the condition of being deeply involved

their entanglement in politics</strong>

<i>Synonyms: mesh(es); morass; net; noose; quagmire; quicksand; snare; tanglement; toil(s); trap; web; </i>

***

<h3>abomination</h3><strong>n. 厌恶；憎恨；令人厌恶的事物</strong>

<strong>1 : something regarded with disgust or hatred : something abominable
considered war an abomination</strong>

<strong>2 : extreme disgust and hatred : loathing
a crime regarded with abomination</strong>

<i>Synonyms: abhorrence; anathema; antipathy; aversion; bête noire; detestation; execration; hate; </i>

***

<h3>retrograde</h3><strong>adj. 倒退的；退化的；次序颠倒的
vi. 逆行；倒退；退步
vt. 使倒退
adv. 倒退地；向后地</strong>

<strong>1 : having or being motion in a direction contrary to that of the general motion of similar bodies and especially east to west among the stars

Saturn is retrograde for another week</strong>

<strong>2 : having or being a direction of rotation or revolution that is clockwise as viewed from the north pole of the sky or a planet

a retrograde orbit</strong>

<strong>3 : moving, occurring, or performed in a backward direction</strong>

<strong>4 : occurring or performed in a direction opposite to the normal or forward direction of conduction or flow: such as</strong>

<strong>5 : occurring along nerve cell processes toward the cell body

retrograde degeneration of nerve fibers
— compare anterograde sense 1a</strong>

<strong>6 — compare anterograde sense 1a</strong>

<strong>7 : occurring opposite to the normal direction or path of blood circulation

retrograde blood flow in veins with incompetent valves
— compare anterograde sense 1b</strong>

<strong>8 — compare anterograde sense 1b</strong>

<strong>9 : contrary to the normal order : inverse</strong>

<strong>10 : tending toward or resulting in a worse or previous state</strong>

<strong>11 : contradictory, opposed</strong>

<strong>12 : characterized by retrogression</strong>

<strong>13 : affecting memories of a period prior to a precipitating event (such as brain injury or disease)

retrograde amnesia
— compare anterograde sense 2</strong>

<strong>14 — compare anterograde sense 2</strong>

<strong>15 : retro
retrograde fashion</strong>

<i>Synonyms: backward; rearward; </i>

***

<h3>grotesque</h3><strong>adj. 怪诞的，奇怪的，可笑的；丑陋奇异的，奇形怪状的
n. （尤指书画中的）奇形怪状的人（或物）；奇异风格；（印刷）畸形字体</strong>

<strong>1 : a style of decorative art characterized by fanciful or fantastic human and animal forms often interwoven with foliage or similar figures that may distort the natural into absurdity, ugliness, or caricature</strong>

<strong>2 : a piece of work in this style

an ornate structure, embellished with grotesques</strong>

<strong>3 : one that is grotesque</strong>

<strong>4 : sans serif</strong>

<i>Synonyms: grotesquerie; grotesquery; monster; monstrosity; ogre; </i>

***

<h3>pliable</h3><strong>adj. 柔韧的；柔软的；圆滑的；易曲折的</strong>

<strong>1 : supple enough to bend freely or repeatedly without breaking</strong>

<strong>2 : yielding readily to others : complaisant</strong>

<strong>3 : adjustable to varying conditions</strong>

<i>Synonyms: bendy; flexible; limber; lissome; lissom; lithe; lithesome; pliant; supple; willowy; </i>

***

<h3>preordain</h3><strong>vt. 注定；预先决定</strong>

<strong>1 : to decree or ordain in advance : foreordain</strong>

<i>Synonyms: destine; doom; fate; foredoom; foreordain; ordain; predestine; predetermine; </i>

***

<h3>shoddy</h3><strong>adj. 劣质的，粗制滥造的；卑鄙的，不正当的
n. 长弹毛（织物），软再生毛（织物）</strong>

<strong>1 : a reclaimed wool from materials that are not felted that is of better quality and longer staple than mungo</strong>

<strong>2 : a fabric often of inferior quality manufactured wholly or partly from reclaimed wool</strong>

<strong>3 : inferior, imitative, or pretentious articles or matter</strong>

<strong>4 : pretentious vulgarity</strong>

<i>Synonyms: bad; bargain-basement; bum; cheap; cheapjack; cheesy; coarse; common; crappy; cut-rate; el cheapo; execrable; gimcrack; inferior; junky; lousy; low-grade; low-rent; mediocre; miserable; poor; rotten; rubbishy; schlock; schlocky; second-rate; sleazy; terrible; trashy; trumpery; wretched; </i>

***

<h3>mesmerizing</h3><strong>施催眠术
迷惑（mesmerize的现在分词）
有吸引力的，有魅力的</strong>

<strong>1 : to subject to mesmerism</strong>

<strong>2 : hypnotize</strong>

<strong>3 : spellbind
I found myself mesmerized by the grandiosity of it all— Arnold Plotnick The crowd was mesmerized by the acrobats.</strong>

<i>Synonyms: arrest; bedazzle; catch up; enchant; enthrall; enthral; fascinate; grip; hypnotize; spellbind; </i>

***

<h3>elegiac</h3><strong>adj. 挽歌的；哀悼的，哀伤的
n. 挽歌，哀歌</strong>

<strong>1 : of, relating to, or consisting of two dactylic hexameter lines the second of which lacks the arsis in the third and sixth feet</strong>

<strong>2 : written in or consisting of elegiac couplets</strong>

<strong>3 : noted for having written poetry in such couplets</strong>

<strong>4 : of or relating to the period in Greece about the seventh century b.c. when poetry written in such couplets flourished</strong>

<strong>5 : of, relating to, or comprising elegy or an elegy</strong>

<strong>6 : expressing sorrow often for something now past</strong>

<i>Synonyms: black; bleak; cheerless; chill; Cimmerian; cloudy; cold; comfortless; dark; darkening; depressing; depressive; desolate; dire; disconsolate; dismal; drear; dreary; dreich; forlorn; funereal; gloomy; glum; godforsaken; gray; grey; lonely; lonesome; lugubrious; miserable; morbid; morose; murky; plutonian; saturnine; sepulchral; solemn; somber; sombre; sullen; sunless; tenebrific; tenebrous; wretched; </i>

***

<h3>anathema</h3><strong>n. 诅咒；革出教门；被诅咒者；令人厌恶的人</strong>

<strong>1 : one that is cursed by ecclesiastical authority</strong>

<strong>2 : someone or something intensely disliked or loathed

—usually used as a predicate nominative … this notion was anathema to most of his countrymen.— Stephen Jay Gould</strong>

<strong>3 : a ban or curse solemnly pronounced by ecclesiastical authority and accompanied by excommunication</strong>

<strong>4 : the denunciation of something as accursed</strong>

<strong>5 : a vigorous denunciation : curse</strong>

<i>Synonyms: ban; curse; execration; imprecation; malediction; malison; winze; </i>

***

<h3>curt</h3><strong>adj. 简短而失礼的，唐突无礼的；简略的，简要的；草率的
n. (Curt) （美）库尔特（人名）</strong>

<strong>1 : sparing of words : terse
wrote curt precise sentences</strong>

<strong>2 : marked by rude or peremptory (see peremptory sense 3) shortness : brusque
a curt refusal In a curt voice, he ordered the server to bring him a glass of water.</strong>

<strong>3 : shortened in linear dimension</strong>

<i>Synonyms: abrupt; bluff; blunt; brusque; brusk; crusty; downright; short; short-spoken; snippy; unceremonious; </i>

***

<h3>long-winded</h3><strong>adj. 冗长的；罗嗦的；气长的</strong>

<strong>1 : tediously long in speaking or writing</strong>

<strong>2 : not easily subject to loss of breath</strong>

<i>Synonyms: circuitous; circumlocutory; diffuse; garrulous; logorrheic; pleonastic; prolix; rambling; verbose; windy; wordy; </i>

***

<h3>implacable</h3><strong>adj. 不能安抚的；难和解的；不能缓和的</strong>

<strong>1 : not placable : not capable of being appeased, significantly changed, or mitigated

an implacable enemy</strong>

<i>Synonyms: adamant; adamantine; bullheaded; dogged; hard; hard-nosed; hardened; hardheaded; headstrong; immovable; inconvincible; inflexible; intransigent; mulish; obdurate; obstinate; opinionated; ossified; pat; pertinacious; perverse; pigheaded; self-opinionated; self-willed; stiff-necked; stubborn; unbending; uncompromising; unrelenting; unyielding; willful; wilful; </i>

***

<h3>ingratiating</h3><strong>adj. 逢迎的，讨好的；迷人的，吸引人的</strong>

<strong>1 : capable of winning favor : pleasing
an ingratiating smile</strong>

<strong>2 : intended or adopted in order to gain favor : flattering</strong>

<i>Synonyms: disarming; endearing; insinuating; winning; winsome; </i>

***

<h3>vitriolic</h3><strong>adj. 硫酸的；刻薄的</strong>

<strong>1 : something felt to resemble vitriol especially in caustic quality</strong>

<strong>2 : virulence of feeling or of speech</strong>

<strong>3 : a sulfate of any of various metals (such as copper, iron, or zinc)</strong>

<strong>4 : a glassy hydrate of such a sulfate</strong>

<strong>5 : oil of vitriol</strong>

<i>Synonyms: acidity; acidness; acridity; acridness; acrimony; asperity; bile; bitterness; cattiness; corrosiveness; mordancy; tartness; virulence; virulency; </i>

***

<h3>unctuous</h3><strong>adj. 油质的；虚情假意的；油腔滑调的</strong>

<strong>1 : having, revealing, or marked by a smug, ingratiating, and false earnestness or spirituality</strong>

<strong>2 : fatty, oily</strong>

<strong>3 : smooth and greasy in texture or appearance</strong>

<strong>4 : plastic
fine unctuous clay</strong>

<i>Synonyms: artificial; backhanded; counterfeit; double; double-dealing; double-faced; fake; feigned; hypocritical; insincere; Janus-faced; jive; left-handed; lip; mealy; mealymouthed; Pecksniffian; phony; phoney; phony-baloney; phoney-baloney; pretended; two-faced; </i>

***

<h3>self-effacing</h3><strong>adj. 谦让的；谦逊的；不出风头的</strong>

<strong>1 : having or showing a tendency to make oneself modestly or shyly inconspicuous

His passions and faith run soul-deep, his gentle wit is always self-effacing and never insulting …— Don Gaetz … she was an extremely self-effacing, private person who never sought the limelight and did not leave the usual artifacts that enable a biographer to reconstruct life patterns.— Douglas R. McManis</strong>

***

<h3>panegyric</h3><strong>n. 颂词，赞颂</strong>

<strong>1 : a eulogistic oration or writing

composed a panegyric about Tchaikovsky for the concert program</strong>

<strong>2 : formal or elaborate praise</strong>

<i>Synonyms: accolade; citation; commendation; dithyramb; encomium; eulogium; eulogy; homage; hymn; paean; salutation; tribute; </i>

***

<h3>vainglorious</h3><strong>adj. 虚荣心强的；非常自负的</strong>

<strong>1 : marked by vainglory : boastful</strong>

<i>Synonyms: assured; biggety; biggity; bigheaded; complacent; conceited; consequential; egoistic; egoistical; egotistic; egotistical; important; overweening; pompous; prideful; proud; self-conceited; self-important; self-opinionated; self-satisfied; smug; stuck-up; swellheaded; vain; </i>

***

<h3>nuanced</h3><strong>adj. 微妙的；具有细微差别的
v. 精确细腻地表演；细致入微地描绘（nuance的过去分词）</strong>

<strong>1 : having nuances : having or characterized by subtle and often appealingly complex qualities, aspects, or distinctions (as in character or tone)

a nuanced performance Whenever the movie focusses on Van Doren and Goodwin and Stempel, it treats them as nuanced human beings. But other characters in the film … are sketched less fully.— Ken Auletta And in this rare instance, the family-friendly franchise isn't a bland, pabulum compromise but a more inspired, nuanced alternative.— Mike Flaherty This more psychologically nuanced self-examination requires that we honestly challenge our beliefs and summon the courage to act on that information, which may lead to fresh ways of thinking about our lives and our goals.— Camille Sweeney and Josh Gosfield</strong>

***

<h3>pry</h3><strong>vt. 撬动，撬开
vi. 刺探，探查
n. 窥探；杠杆</strong>

<strong>1 : to look closely or inquisitively</strong>

<strong>2 : to make a nosy or presumptuous inquiry</strong>

***

<h3>preempted</h3><strong>vt. 先占；先取；以先买权获得</strong>

<strong>1 : to acquire (something, such as land) by preemption</strong>

<strong>2 : to seize upon to the exclusion of others : take for oneself

the movement was then preempted by a lunatic fringe</strong>

<strong>3 : to replace with something considered to be of greater value or priority : take precedence over

the program did not appear, having been preempted by a baseball game— Robert MacNeil</strong>

<strong>4 : to replace or supersede (a law) or bar (an action) by the doctrine of preemption
In so doing, the Court held that Congress did not intend to preempt common law contract claims. — Charas v. Trans World Airlines, Inc.</strong>

<strong>5 : to gain a commanding or preeminent place in</strong>

<strong>6 : to prevent from happening or taking place : forestall, preclude</strong>

<strong>7 : to make a preemptive bid in bridge</strong>

<i>Synonyms: appropriate; arrogate; commandeer; convert; expropriate; pirate; press; seize; take over; usurp; </i>

***

<h3>topple</h3><strong>vi. 倾倒；倒塌；摇摇欲坠
vt. 推翻；颠覆；使倒塌
n. (Topple)人名；(英)托佩尔</strong>

<strong>1 : to fall from or as if from being top-heavy</strong>

<strong>2 : to cause to topple</strong>

<strong>3 : overthrow sense 2
topple a dictator</strong>

<strong>4 : defeat sense 1</strong>

<i>Synonyms: fall; slip; stumble; trip; tumble; </i>

***

<h3>traumatic</h3><strong>adj. （心理）创伤的
adj. （生理）外伤的</strong>

<strong>1 : psychologically or emotionally stressful in a way that can lead to serious mental and emotional problems

The report … revealed that the onset of bulimic behavior tends to be associated with voluntary dieting and with traumatic events such as the loss of a loved one.— Ralph Heussner He's been open and frank about his traumatic childhood scarred by violence and domestic abuse.— Stephen Milton


broadly
: causing distress or anxiety 
The traumatic experience of the Wall Street crash and the long depression of the 1930s has gradually faded from memory …— Jack RevellThe arrival of mass tourism in a previously unspoilt area … was certainly traumatic.</strong>

<strong>2 : causing distress or anxiety</strong>

***

<h3>dismal</h3><strong>adj. 凄凉的，忧郁的；阴沉的，沉闷的；差劲的；可怜的</strong>

<strong>1 : showing or causing gloom (see gloom entry 2 sense 2) or depression

the dismal prison twilight— Charles Dickens</strong>

<strong>2 : lacking merit : particularly bad

a dismal performance</strong>

<strong>3 : disastrous, dreadful</strong>

<i>Synonyms: black; bleak; cheerless; chill; Cimmerian; cloudy; cold; comfortless; dark; darkening; depressing; depressive; desolate; dire; disconsolate; drear; dreary; dreich; elegiac; elegiacal; forlorn; funereal; gloomy; glum; godforsaken; gray; grey; lonely; lonesome; lugubrious; miserable; morbid; morose; murky; plutonian; saturnine; sepulchral; solemn; somber; sombre; sullen; sunless; tenebrific; tenebrous; wretched; </i>

***

<h3>earnest</h3><strong>adj. 认真的，热心的；重要的
n. 认真；定金；诚挚
n. (Earnest)人名；(英)欧内斯特</strong>

<strong>1 : characterized by or proceeding from an intense and serious state of mind</strong>

<strong>2 : grave, important</strong>

<i>Synonyms: grave; humorless; no-nonsense; po-faced; sedate; serious; severe; sober; sobersided; solemn; staid; uncomic; unsmiling; weighty; </i>

***

<h3>rigor</h3><strong>n. （尤指发热之初或高潮时的）寒战；严密，严格（rigour 的美式拼法）
n. (Rigor) （菲、美）里戈尔（人名）</strong>

<strong>1 : harsh inflexibility in opinion, temper, or judgment : severity
The least one might observe is that this muddle of moralism and laxity, extreme rigor and casual permissiveness, arduous altruism and nonchalant selfishness, has consequences.— Peter Berkowitz</strong>

<strong>2 : the quality of being unyielding or inflexible : strictness
Their abandonment of this terminology is part of an effort to soften the rigor of some of the rigid consequences thought to flow from the mechanical application of this dichotomy.— John D. Calamari and Joseph M. Perillo</strong>

<strong>3 : severity of life : austerity
… a moral rigor and growth that might help this country.— Stanley Kauffmann</strong>

<strong>4 : an act or instance of strictness, severity, or cruelty

… the humanist must recognize the normality, the practical necessity of the very rigors he is trying to soften and correct.— Hermann J. Muller</strong>

<strong>5 : a tremor caused by a chill

This young woman presented at the hospital with severe abdominal pain and signs of infection, including fever, rigor, and leukocytosis.— Robert E. Scully et al.</strong>

<strong>6 : a condition that makes life difficult, challenging, or uncomfortable</strong>

<strong>7 : extremity of cold</strong>

<i>Synonyms: adversity; asperity; difficulty; hardness; hardship; </i>

***

<h3>corroboration</h3><strong>n. 确证，证实；确证的事实</strong>

<strong>1 : to support with evidence or authority : make more certain</strong>

<i>Synonyms: argue; attest; authenticate; bear out; certify; confirm; substantiate; support; validate; verify; vindicate; </i>

***

<h3>suspense</h3><strong>n. 悬念；悬疑；焦虑；悬而不决</strong>

<strong>1 : the state of being suspended : suspension</strong>

<strong>2 : mental uncertainty : anxiety</strong>

<strong>3 : pleasant excitement as to a decision or outcome

a novel of suspense</strong>

<strong>4 : the state or character of being undecided or doubtful : indecisiveness</strong>

<i>Synonyms: abeyance; cold storage; deep freeze; doldrums; dormancy; holding pattern; latency; moratorium; quiescence; suspended animation; suspension; </i>

***

<h3>gadfly</h3><strong>n. [昆] 牛虻；讨厌的人</strong>

<strong>1 : any of various flies (such as a horsefly, botfly, or warble fly) that bite or annoy livestock</strong>

<strong>2 : a person who stimulates or annoys other people especially by persistent criticism

a political gadfly</strong>

<i>Synonyms: annoyance; annoyer; bother; gnawer; nudnik; nudnick; nuisance; pain; persecutor; pest; tease; teaser; </i>

***

<h3>pandemonium</h3><strong>n. 一片混乱；闹哄哄的场所</strong>

<strong>1 : a wild uproar (as because of anger or excitement in a crowd of people)

Pandemonium erupted in the courtroom when the verdict was announced. The game is stopped on account of pandemonium: players and spectators are screaming and staggering around the court …— Darcy Frey


also
: a chaotic situation 
It is obvious that pandemonium would exist at most uncontrolled airports if every pilot did not conscientiously follow the traffic pattern.— Joseph W. Benkert</strong>

<strong>2 : a chaotic situation</strong>

<strong>3 : the capital of Hell in Milton's Paradise Lost</strong>

<strong>4 : the infernal regions : hell
the demons of Pandemonium</strong>

***

<h3>foment</h3><strong>vt. 煽动；挑起；热敷</strong>

<strong>1 : to promote the growth or development of : rouse, incite
foment a rebellion was accused of fomenting a riot</strong>

<i>Synonyms: abet; brew; ferment; incite; instigate; pick; provoke; raise; stir (up); whip (up); </i>

***

<h3>far-fetched</h3><strong>adj. 牵强附会的</strong>

<strong>1 : brought from a remote time or place</strong>

<strong>2 : not easily or naturally deduced or introduced : improbable
a far-fetched story</strong>

<i>Synonyms: doubtful; dubious; flimsy; improbable; questionable; unapt; unlikely; </i>

***

<h3>dub</h3><strong>v. 给……起别名（或绰号）；封……为爵士；装上（假蝇）做钓饵；将（软毛、羊毛等材料）掺入鱼饵；在（皮革上）涂油；译制；配音；复制；合成；（高尔夫）打糟（球的一击）；付清，捐款
n. 配音，配乐；混录音乐；新手；强节奏音乐，强节拍诗歌；鼓声；笨蛋
n. (Dub) （乌克兰、美）杜布（人名）</strong>

<strong>1 : to confer knighthood on

was dubbed Sir Philip</strong>

<strong>2 : to call by a distinctive title, epithet, or nickname

Critics have dubbed him the new king of rock 'n' roll.</strong>

<strong>3 : to trim or remove the comb (see comb entry 1 sense 2a) and wattles of</strong>

<strong>4 : to hit (a ball or shot) poorly</strong>

<strong>5 : to execute poorly

a dubbed attempt</strong>

***

<h3>magnanimity</h3><strong>n. 宽宏大量；慷慨</strong>

<strong>1 : the quality of being magnanimous : loftiness of spirit enabling one to bear trouble calmly, to disdain meanness and pettiness, and to display a noble generosity

He had the magnanimity to forgive her for lying about him.</strong>

<strong>2 : a magnanimous act

the great magnanimities between soldiers— Katharine Tynan</strong>

***

<h3>profligacy</h3><strong>n. 肆意挥霍；放荡；浪费</strong>

<strong>1 : the quality or state of being profligate</strong>

<i>Synonyms: corruption; debauchery; depravity; immorality; iniquitousness; iniquity; libertinage; libertinism; licentiousness; sin; vice; </i>

***

<h3>demur</h3><strong>vi. 反对；抗辩；提出异议
n. 异议；反对
n. (Demur)人名；(德)德穆尔；(法)德米尔</strong>

<strong>1 : delay, hesitate</strong>

<strong>2 : to file a demurrer</strong>

<strong>3 : to take exception : object

—often used with to or at it would seem hazardous to demur to a proposition which is so widely accepted— Samuel Alexander</strong>

<i>Synonyms: except; expostulate; kick; object; protest; remonstrate; </i>

***

<h3>expatriate</h3><strong>v. 移居国外，定居国外；流放，放逐
n. 移居国外者，侨民；被流放者
adj. 移居国外的；被流放国外的</strong>

<strong>1 : banish, exile</strong>

<strong>2 : to withdraw (oneself) from residence in or allegiance to one's native country</strong>

<strong>3 : to leave one's native country to live elsewhere</strong>

<strong>4 : to renounce allegiance to one's native country</strong>

<i>Synonyms: banish; deport; displace; exile; relegate; transport; </i>

***

<h3>qualm</h3><strong>n. 疑虑；不安</strong>

<strong>1 : a feeling of uneasiness about a point especially of conscience or propriety

had no qualms about asking for their help It was about an enduring secular world where people did unspeakable things, seemingly without qualm and without any grave consequences to themselves.— Jim Holt</strong>

<strong>2 : a sudden feeling of usually disturbing emotion (such as doubt or fear)

I had a qualm of absolute horror, and shuddered; and then the emotion was immediately repressed or suppressed.— Oliver Sacks</strong>

<strong>3 : a sudden attack of illness, faintness, or nausea

The doctor seemed seized with a qualm of faintness.— Robert Louis Stevenson</strong>

<i>Synonyms: compunction; misgiving; scruple; </i>

***

<h3>scornful</h3><strong>adj. 轻蔑的</strong>

<strong>1 : full of scorn : contemptuous</strong>

<i>Synonyms: abhorrent; contemptuous; disdainful; </i>

***

<h3>deferential</h3><strong>adj. 恭敬的；惯于顺从的</strong>

<strong>1 : showing or expressing respect and high regard due a superior or an elder : showing or expressing deference
listened with deferential attention to his grandfather deferential to the judge's decision</strong>

<i>Synonyms: deferent; dutiful; regardful; respectful; </i>

***

<h3>epitome</h3><strong>n. 缩影；摘要；象征</strong>

<strong>1 : a typical or ideal example : embodiment
the British monarchy itself is the epitome of tradition— Richard Joseph</strong>

<strong>2 : a summary of a written work</strong>

<strong>3 : a brief presentation or statement of something</strong>

<strong>4 : brief or miniature form

—usually used with in</strong>

<i>Synonyms: abstract; breviary; brief; capsule; conspectus; digest; encapsulation; inventory; outline; précis; recap; recapitulation; résumé; resume; roundup; run-through; rundown; sum; sum-up; summa; summarization; summary; summing-up; synopsis; wrap-up; </i>

***

<h3>snobbish</h3><strong>adj. 势利的</strong>

<strong>1 : being, characteristic of, or befitting a snob

a snobbish attitude</strong>

<i>Synonyms: aristocratic; elitist; high-hat; persnickety; potty; ritzy; snobby; snooty; snotty; toffee-nosed; </i>

***

<h3>collegial</h3><strong>adj. 学院的；社团的；大学生的</strong>

<strong>1 : collegiate sense 2</strong>

<strong>2 : marked by power or authority vested equally in each of a number of colleagues
There was an increasing tendency to turn from collegial to one-man management.— Merle Fainsod</strong>

<strong>3 : characterized by equal sharing of authority especially by Roman Catholic bishops

a collegial church</strong>

<strong>4 : marked by camaraderie among colleagues

collegial relationships among faculty members</strong>

<i>Synonyms: amicable; bonhomous; buddy-buddy; chummy; companionable; comradely; cordial; friendly; genial; hail-fellow; hail-fellow-well-met; hearty; matey; neighborly; palsy; palsy-walsy; warm; warmhearted; </i>

***

<h3>perspicacity</h3><strong>n. 洞察力；聪颖；睿智</strong>

<strong>1 : of acute mental vision or discernment : keen</strong>

***

<h3>unrelenting</h3><strong>adj. 无情的；不屈不挠的；不松懈的</strong>

<strong>1 : not softening or yielding in determination : hard, stern
an unrelenting leader</strong>

<strong>2 : not letting up or weakening in vigor or pace : constant
the unrelenting struggle</strong>

<i>Synonyms: adamant; adamantine; bullheaded; dogged; hard; hard-nosed; hardened; hardheaded; headstrong; immovable; implacable; inconvincible; inflexible; intransigent; mulish; obdurate; obstinate; opinionated; ossified; pat; pertinacious; perverse; pigheaded; self-opinionated; self-willed; stiff-necked; stubborn; unbending; uncompromising; unyielding; willful; wilful; </i>

***

<h3>extirpate</h3><strong>v. 根除；彻底毁坏</strong>

<strong>1 : to destroy completely : wipe out</strong>

<strong>2 : to pull up by the root</strong>

<strong>3 : to cut out by surgery</strong>

<i>Synonyms: abolish; annihilate; black out; blot out; cancel; clean (up); efface; eradicate; erase; expunge; exterminate; liquidate; obliterate; root (out); rub out; snuff (out); stamp (out); sweep (away); wipe out; </i>

***

<h3>elation</h3><strong>n. 兴高采烈，得意洋洋</strong>

<strong>1 : the quality or state of being elated</strong>

<strong>2 : pathological euphoria</strong>

<i>Synonyms: cloud nine; ecstasy; elatedness; euphoria; exhilaration; heaven; high; intoxication; paradise; rapture; rhapsody; seventh heaven; swoon; transport; </i>

***

<h3>unqualified</h3><strong>adj. 不合格的；无资格的；不胜任的；不受限制的；无条件的；绝对的</strong>

<strong>1 : not fit : not having requisite qualifications</strong>

<strong>2 : not modified or restricted by reservations : complete
an unqualified denial</strong>

<i>Synonyms: absolute; all-out; arrant; blank; blooming; bodacious; categorical; categoric; clean; complete; consummate; cotton-picking; crashing; damn; damned; dead; deadly; definite; downright; dreadful; fair; flat; flat-out; out-and-out; outright; perfect; plumb; profound; pure; rank; regular; sheer; simple; stark; stone; straight-out; thorough; thoroughgoing; total; unadulterated; unalloyed; unconditional; unmitigated; utter; very; </i>

***

<h3>crabbed</h3><strong>adj. 暴躁的；难懂的；晦涩的；潦草的
v. 抱怨；侧航（crab的过去分词）</strong>

<strong>1 : marked by a forbidding moroseness

a crabbed view of human nature</strong>

<strong>2 : difficult to read or understand

crabbed handwriting</strong>

***

<h3>indecorous</h3><strong>adj. 不合礼节的，不得体的</strong>

<strong>1 : not decorous : conflicting with accepted standards of good conduct or good taste</strong>

<i>Synonyms: amiss; graceless; improper; inapposite; inappropriate; inapt; incongruous; incorrect; inept; infelicitous; malapropos; perverse; unapt; unbecoming; unfit; unhappy; unseemly; unsuitable; untoward; wrong; </i>

***

<h3>pedigree</h3><strong>n. 血统；家谱
adj. 纯种的</strong>

<strong>1 : a register recording a line of ancestors

The pedigree traces the family back to the 18th century.</strong>

<strong>2 : an ancestral line : lineage
That horse has an impressive pedigree.</strong>

<strong>3 : the origin and the history of something

Democracy's pedigree stretches back to ancient Greece.</strong>

<strong>4 : background, history</strong>

<strong>5 : a distinguished ancestry

actions spoke louder than pedigrees in the trenches— Dixon Wecter</strong>

<strong>6 : the recorded purity of breed of an individual or strain

vouch for a horse's pedigree</strong>

<i>Synonyms: ancestry; birth; blood; bloodline; breeding; descent; extraction; family tree; genealogy; line; lineage; origin; parentage; stock; strain; </i>

***

<h3>anatomize</h3><strong>vt. 解剖；仔细分析</strong>

<strong>1 : to cut in pieces in order to display or examine the structure and use of the parts : dissect</strong>

<strong>2 : analyze</strong>

<i>Synonyms: analyze; assay; break down; cut; deconstruct; dissect; </i>

***

<h3>fickleness</h3><strong>n. 浮躁；变化无常</strong>

<strong>1 : marked by lack of steadfastness, constancy, or stability : given to erratic changeableness</strong>

<i>Synonyms: capricious; changeable; changeful; flickery; fluctuating; fluid; inconsistent; inconstant; mercurial; mutable; skittish; temperamental; uncertain; unpredictable; unsettled; unstable; unsteady; variable; volatile; </i>

***

<h3>incorporate</h3><strong>vt. 包含，吸收；体现；把……合并
vi. 合并；混合；组成公司
adj. 合并的；一体化的；组成公司的</strong>

<strong>1 : to unite or work into something already existent so as to form an indistinguishable whole</strong>

<strong>2 : to blend or combine thoroughly</strong>

<strong>3 : to form into a legal corporation</strong>

<strong>4 : to admit to membership in a corporate body</strong>

<strong>5 : to give material form to : embody</strong>

<strong>6 : to unite in or as one body</strong>

<strong>7 : to form or become a corporation</strong>

<i>Synonyms: absorb; assimilate; co-opt; embody; integrate; </i>

***

<h3>hallowed</h3><strong>adj. 神圣的，神圣化的</strong>

<strong>1 : holy, consecrated
the church stands on hallowed ground</strong>

<strong>2 : sacred, revered
the university's hallowed halls hallowed customs</strong>

<i>Synonyms: revered; reverend; sacred; venerable; venerated; </i>

***

<h3>quell</h3><strong>vt. 平息；镇压；减轻；消除
n. (Quell)人名；(捷)奎尔；(西)克利</strong>

<strong>1 : to thoroughly overwhelm and reduce to submission or passivity

quell a riot</strong>

<strong>2 : quiet, pacify
quell fears</strong>

<i>Synonyms: clamp down (on); crack down (on); crush; put down; quash; repress; silence; slap down; snuff (out); squash; squelch; subdue; suppress; </i>

***

<h3>inchoate</h3><strong>adj. 早期的；刚开始的；未充分发展的
vt. 开始
vi. 开始</strong>

<strong>1 : being only partly in existence or operation : incipient</strong>

<strong>2 : imperfectly formed or formulated : formless, incoherent</strong>

<i>Synonyms: aborning; budding; inceptive; incipient; nascent; </i>

***

<h3>ungainly</h3><strong>adj. 笨拙的；不雅的
adv. 笨拙地；不雅地</strong>

<strong>1 : lacking in smoothness or dexterity : clumsy
ungainly movements</strong>

<strong>2 : hard to handle : unwieldy
an ungainly contraption</strong>

<strong>3 : having an awkward appearance

a large ungainly bird</strong>

<i>Synonyms: awkward; bunglesome; clumsy; clunky; cranky; cumbersome; cumbrous; ponderous; unhandy; unwieldy; </i>

***

<h3>preternatural</h3><strong>adj. 超自然的；异常的，不可思议的</strong>

<strong>1 : existing outside of nature</strong>

<strong>2 : exceeding what is natural or regular : extraordinary
wits trained to preternatural acuteness by the debates— G. L. Dickinson</strong>

<strong>3 : inexplicable by ordinary means</strong>

<strong>4 : psychic</strong>

<i>Synonyms: aberrant; aberrated; abnormal; anomalous; atypical; especial; exceeding; exceptional; extraordinaire; extraordinary; freak; odd; peculiar; phenomenal; rare; singular; uncommon; uncustomary; unique; unusual; unwonted; </i>

***

<h3>prodigious</h3><strong>adj. 惊人的，异常的，奇妙的；巨大的</strong>

<strong>1 : causing amazement or wonder</strong>

<strong>2 : extraordinary in bulk, quantity, or degree : enormous</strong>

<strong>3 : resembling or befitting a prodigy : strange, unusual</strong>

<strong>4 : being an omen : portentous</strong>

<i>Synonyms: amazing; astonishing; astounding; awesome; awful; eye-opening; fabulous; marvelous; marvellous; miraculous; portentous; staggering; stunning; stupendous; sublime; surprising; wonderful; wondrous; </i>

***

<h3>derogatory</h3><strong>adj. 贬低的，不敬的，持批评态度的</strong>

<strong>1 : expressive of a low opinion : disparaging
derogatory remarks a derogatory term</strong>

<strong>2 : detracting from the character or standing of something

—often used with to, towards, or of … abstained from saying a word derogatory to his new friend's religion …— Anthony Trollope</strong>

<i>Synonyms: belittling; contemptuous; decrying; degrading; demeaning; denigrative; denigratory; deprecatory; depreciative; depreciatory; derisory; derogative; detractive; disdainful; disparaging; pejorative; scornful; slighting; uncomplimentary; </i>

***

<h3>boon</h3><strong>n. 恩惠；福利；利益
adj. 愉快的；慷慨的
n. (Boon)人名；(英)布恩；(东南亚国家华语)文；(瑞典、德)博恩；(泰)汶</strong>

<strong>1 : benefit, favor</strong>

<strong>2 : one that is given in answer to a request</strong>

<i>Synonyms: advantage; aid; asset; benefit; help; </i>

***

<h3>soothsayer</h3><strong>n. 算命者；预言者</strong>

<strong>1 : a person who predicts the future by magical, intuitive, or more rational means : prognosticator</strong>

<i>Synonyms: augur; diviner; forecaster; foreseer; foreteller; fortune-teller; futurist; prognosticator; prophesier; prophet; seer; visionary; </i>

***

<h3>evangelist</h3><strong>n. 福音传道者；圣经新约福音书的作者</strong>

<strong>1 : a writer of any of the four Gospels</strong>

<strong>2 : a person who evangelizes</strong>

<strong>3 : a Protestant minister or layman who preaches at special services</strong>

<strong>4 : an enthusiastic advocate

an evangelist for physical fitness</strong>

***

<h3>recalcitrant</h3><strong>adj. 反抗的；反对的；顽强的
n. 顽抗者；不服从的人</strong>

<strong>1 : obstinately defiant of authority or restraint</strong>

<strong>2 : difficult to manage or operate</strong>

<strong>3 : not responsive to treatment</strong>

<strong>4 : resistant
this subject is recalcitrant both to observation and to experiment— G. G. Simpson</strong>

<i>Synonyms: balky; contrary; contumacious; defiant; disobedient; froward; incompliant; insubordinate; intractable; obstreperous; rebel; rebellious; recusant; refractory; restive; ungovernable; unruly; untoward; wayward; willful; wilful; </i>

***

<h3>agreeable</h3><strong>adj. 令人愉快的；适合的；和蔼可亲的</strong>

<strong>1 : pleasing to the mind or senses especially as according well with one's tastes or needs

an agreeable companion an agreeable change</strong>

<strong>2 : ready or willing to agree or consent

was agreeable to the plan</strong>

<strong>3 : being in harmony : consonant</strong>

<strong>4 : able to be accepted : acceptable

—used with to a plan that is agreeable to everyone</strong>

<i>Synonyms: all right; alright; copacetic; copasetic; ducky; fine; good; hunky-dory; jake; OK; okay; palatable; satisfactory; </i>

***

<h3>obtuse</h3><strong>adj. 迟钝的；圆头的；不锋利的</strong>

<strong>1 : not pointed or acute : blunt</strong>

<strong>2 : exceeding 90 degrees but less than 180 degrees</strong>

<strong>3 : having an obtuse angle

an obtuse triangle
— see triangle illustration</strong>

<strong>4 — see triangle illustration</strong>

<strong>5 : rounded at the free end</strong>

<strong>6 : lacking sharpness or quickness of sensibility or intellect : insensitive, stupid
He is too obtuse to take a hint.</strong>

<strong>7 : difficult to comprehend : not clear or precise in thought or expression

It is also, unfortunately, ill-written, and at times obtuse and often trivial.— Shirley Hazzard</strong>

<i>Synonyms: blunt; blunted; dull; dulled; </i>

***

<h3>apparel</h3><strong>n. 服装；衣服
vt. 给…穿衣</strong>

<strong>1 : personal attire : clothing of a particular kind

dressed in fine apparel

—used chiefly in U.S. English to refer to clothing that is being sold in stores a new line of women's apparelathletic apparel</strong>

<strong>2 : the equipment (such as sails and rigging) of a ship</strong>

<strong>3 : something that clothes or adorns

the bright apparel of spring</strong>

<i>Synonyms: attire; clobber; clothes; clothing; costumery; dress; duds; garments; gear; habiliment(s); habit; rags; raiment; rig; rigging; threads; toggery; togs; vestiary; vestments; vesture; wear; wearables; weeds; </i>

***

<h3>ascension</h3><strong>n. 上升；耶稣升天
n. (Ascension)人名；(西)阿森西翁</strong>

<strong>1 : the act or process of ascending</strong>

<i>Synonyms: ascent; climb; rise; rising; soar; </i>

***

<h3>futurity</h3><strong>n. 未来；来世；后世的人</strong>

<strong>1 : time to come : future</strong>

<strong>2 : the quality or state of being future</strong>

<strong>3 : future events or prospects</strong>

<strong>4 : a horse race usually for two-year-olds in which the competitors are nominated at birth or before</strong>

<strong>5 : a race or competition for which entries are made well in advance of the event</strong>

<i>Synonyms: by-and-by; future; hereafter; offing; tomorrow; </i>

***

<h3>staggering</h3><strong>adj. 惊人的，令人震惊的</strong>

<strong>1 : so great as to cause one to stagger : astonishing, overwhelming
a staggering feat staggering medical bills</strong>

<i>Synonyms: amazing; astonishing; astounding; awesome; awful; eye-opening; fabulous; marvelous; marvellous; miraculous; portentous; prodigious; stunning; stupendous; sublime; surprising; wonderful; wondrous; </i>

***

<h3>imperilled</h3><strong>adj. 处于危险中
v. 危害；使…陷于危险（imperil的过去分词形式）</strong>

<strong>1 : to bring into peril : endanger</strong>

<i>Synonyms: adventure; compromise; endanger; gamble (with); hazard; jeopard; jeopardize; menace; peril; risk; venture; </i>

***

<h3>obedient</h3><strong>adj. 顺从的，服从的；孝顺的</strong>

<strong>1 : submissive to the restraint or command of authority : willing to obey

an obedient child an obedient dog obedient to those whom he feared— A. N. Wilson</strong>

<i>Synonyms: amenable; biddable; compliant; conformable; docile; law-abiding; submissive; tractable; </i>

***

<h3>inoculate</h3><strong>vt. [医] 接种；嫁接；灌输</strong>

<strong>1 : to introduce a microorganism into

inoculate mice with anthrax beans inoculated with nitrogen-fixing bacteria</strong>

<strong>2 : to introduce (something, such as a microorganism) into a suitable situation for growth</strong>

<strong>3 : to introduce immunologically active material (such as an antibody or antigen) into especially in order to treat or prevent a disease

inoculate children against diphtheria</strong>

<strong>4 : to introduce something into the mind of</strong>

<strong>5 : to protect as if by inoculation</strong>

<i>Synonyms: endue; indue; imbue; inculcate; infuse; ingrain; engrain; invest; steep; suffuse; </i>

***

<h3>salvational</h3><strong>灵魂的得救，超度，救度
救世</strong>

<strong>1 : deliverance from the power and effects of sin</strong>

<strong>2 : the agent or means that effects salvation</strong>

<strong>3 : the realization of the supremacy of infinite Mind over all bringing with it the destruction of the illusion of sin, sickness, and death</strong>

<strong>4 : liberation from ignorance or illusion</strong>

<strong>5 : preservation from destruction or failure</strong>

<strong>6 : deliverance from danger or difficulty</strong>

<i>Synonyms: deliverance; rescue; </i>

***

<h3>tendentious</h3><strong>adj. 有偏见的，有倾向的；宣传性的</strong>

<strong>1 : marked by a tendency in favor of a particular point of view : biased</strong>

***

<h3>leaven</h3><strong>n. 酵母；酵素；潜移默化的影响；调和剂
v. 改变；使发酵；影响；使……较有趣；为……增色</strong>

<strong>1 : a substance (such as yeast) used to produce fermentation in dough or a liquid</strong>

<strong>2 : sourdough</strong>

<strong>3 : a material (such as baking powder) used to produce a gas that lightens dough or batter</strong>

<strong>4 : something that modifies or lightens</strong>

***

<h3>disparage</h3><strong>v. 贬低；抨击，诋毁</strong>

<strong>1 : to depreciate (see depreciate sense 1) by indirect means (such as invidious comparison) : speak slightingly about

religious beliefs disparaged as superstition</strong>

<strong>2 : to lower in rank or reputation : degrade</strong>

<i>Synonyms: bad-mouth; belittle; cry down; decry; denigrate; deprecate; depreciate; derogate; diminish; dis; diss; discount; dismiss; kiss off; minimize; play down; poor-mouth; put down; run down; talk down; trash; trash-talk; vilipend; write off; </i>

***

<h3>charlatan</h3><strong>n. 骗子，冒牌内行，江湖郎中</strong>

<strong>1 : quack entry 4 sense 2
charlatans harming their patients with dubious procedures</strong>

<strong>2 : one making usually showy pretenses to knowledge or ability : fraud, faker
a charlatan willing to do and say virtually anything to remain in the spotlight— Alan Brinkley</strong>

<i>Synonyms: fake; faker; fakir; fraud; hoaxer; humbug; impostor; imposter; mountebank; phony; phoney; pretender; quack; quacksalver; ringer; sham; </i>

***

<h3>malingering</h3><strong>n. 装病
v. 装病（malinger 的现在分词）</strong>

<strong>1 : to pretend or exaggerate incapacity or illness (as to avoid duty or work)

His boss suspected him of malingering because of his frequent absences from work.</strong>

***

<h3>retribution</h3><strong>n. 报应；惩罚；报答；报偿</strong>

<strong>1 : recompense, reward</strong>

<strong>2 : the dispensing or receiving of reward or punishment especially in the hereafter</strong>

<strong>3 : something given or exacted in recompense</strong>

<strong>4 : punishment</strong>

<i>Synonyms: payback; reprisal; requital; retaliation; revenge; vengeance; </i>

***

<h3>earnestness</h3><strong>n. 认真；诚挚；正经</strong>

<strong>1 : characterized by or proceeding from an intense and serious state of mind</strong>

<strong>2 : grave, important</strong>

<i>Synonyms: grave; humorless; no-nonsense; po-faced; sedate; serious; severe; sober; sobersided; solemn; staid; uncomic; unsmiling; weighty; </i>

***

<h3>halcyon</h3><strong>adj. 宁静的，田园诗般美好的
n. 翡翠鸟；神翠鸟（神话中的神鸟）
n. (Halcyon) （美）哈尔西恩（人名）</strong>

<strong>1 : characterized by happiness, great success, and prosperity : golden

—often used to describe an idyllic time in the past that is remembered as better than today the halcyon days of youthClassics Illustrated have become pricey nostalgia items for those who grew up in the supposedly halcyon years after World War II.— Donna RichardsonIn those halcyon days of the free trade, the fixed price for carrying a box of tea or bale of tobacco from the coast of Galloway to Edinburgh was fifteen shillings …— Sir Walter Scott</strong>

<strong>2 : calm, peaceful
a halcyon atmosphere</strong>

<strong>3 : prosperous, affluent</strong>

<strong>4 : of or relating to the halcyon (see halcyon entry 2) or its nesting period</strong>

<i>Synonyms: calm; hushed; lown; peaceful; placid; quiet; serene; still; stilly; tranquil; untroubled; </i>

***

<h3>aphorism</h3><strong>n. 格言；警句</strong>

<strong>1 : a concise statement of a principle</strong>

<strong>2 : a terse formulation of a truth or sentiment : adage
the high-minded aphorism, "Let us value the quality of life, not the quantity"</strong>

<strong>3 : an ingeniously terse style of expression : aphoristic language

These are dazzling chapters, packed with perfectly chosen anecdotes and pithy with aphorism.— John Keegan</strong>

<i>Synonyms: adage; apothegm; byword; epigram; maxim; proverb; saw; saying; sententia; word; </i>

***

<h3>riotous</h3><strong>adj. 暴乱的；狂欢的；不受约束的；放荡的；茂盛的</strong>

<strong>1 : of the nature of a riot : turbulent</strong>

<strong>2 : participating in riot</strong>

<strong>3 : abundant, exuberant
the garden was riotous with flowers</strong>

<i>Synonyms: antic; chucklesome; comedic; comic; comical; droll; farcical; funny; hilarious; humoristic; humorous; hysterical; hysteric; killing; laughable; ludicrous; ridiculous; risible; screaming; sidesplitting; uproarious; </i>

***

<h3>superlative</h3><strong>adj. 最高的；最高级的；过度的
n. 最高级；最好的人；最高程度；夸大话</strong>

<strong>1 : of, relating to, or constituting the degree of grammatical comparison that denotes an extreme or unsurpassed level or extent</strong>

<strong>2 : surpassing all others : supreme</strong>

<strong>3 : of very high quality : excellent
superlative work</strong>

<strong>4 : excessive, exaggerated</strong>

<i>Synonyms: A-OK; A1; awesome; bang-up; banner; beautiful; blue-chip; blue-ribbon; boffo; bonny; bonnie; boss; brag; brave; bully; bumper; capital; choice; classic; cool; corking; crackerjack; cracking; dandy; divine; dope; down; dynamite; excellent; fab; fabulous; famous; fantabulous; fantastic; fine; first-class; first-rate; first-string; five-star; four-star; frontline; gangbusters; gangbuster; gilt-edged; gilt-edge; gone; grand; great; groovy; heavenly; high-class; hot; hype; immense; jim-dandy; keen; lovely; marvelous; marvellous; mean; neat; nifty; noble; number one; No. 1; numero uno; out-of-sight; par excellence; peachy; peachy keen; phat; prime; primo; prize; prizewinning; quality; radical; righteous; sensational; slick; splendid; stellar; sterling; superb; superior; supernal; swell; terrific; tip-top; top; top-notch; top-of-the-line; top-shelf; topflight; topping; unsurpassed; wizard; wonderful; </i>

***

<h3>dismantles</h3><strong>vt. 拆除；取消；解散；除掉…的覆盖物
vi. 可拆卸</strong>

<strong>1 : to disconnect the pieces of

will have to dismantle the engine</strong>

<strong>2 : to destroy the integrity or functioning of</strong>

<strong>3 : to strip of dress or covering : divest</strong>

<strong>4 : to strip of furniture and equipment

dismantled the ship before scrapping it</strong>

<i>Synonyms: break down; demount; disassemble; dismember; dismount; knock down; strike; take down; </i>

***

<h3>corral</h3><strong>n. （北美农牧场的）畜栏；（营地中防御用的）车阵
v. 把（马、牛）关进畜栏；把（一群人）集中关在一起；将（车辆）围作车阵；捉住
n. (Corral) （美）科拉尔（人名）</strong>

<strong>1 : a pen or enclosure for confining or capturing livestock

herded the cattle into the corral</strong>

<strong>2 : an enclosure made with wagons for defense of an encampment</strong>

<i>Synonyms: cage; coop; hutch; pen; pound; </i>

***

<h3>encroachment</h3><strong>n. 侵入，侵犯；侵蚀</strong>

<strong>1 : to enter by gradual steps or by stealth into the possessions or rights of another</strong>

<strong>2 : to advance beyond the usual or proper limits

the gradually encroaching sea</strong>

<i>Synonyms: creep; inch; worm; </i>

***

<h3>foray</h3><strong>n. 涉足，初次尝试；突袭；侵略；攻击
vi. 袭击
vt. 劫掠
n. (Foray)人名；(法)福雷</strong>

<strong>1 : a sudden or irregular invasion or attack for war or spoils : raid
a foray into enemy territory attempted sneak and surprise forays— D. D. Eisenhower</strong>

<strong>2 : an initial and often tentative attempt to do something in a new or different field or area of activity

the novelist's foray into nonfiction Torres spearheaded his family's first foray into international wine production by purchasing land in Chile's Curicó Valley in 1979.— Michael Schachner</strong>

<i>Synonyms: descent; incursion; inroad; invasion; irruption; raid; </i>

***

<h3>peninsula</h3><strong>n. 半岛</strong>

<strong>1 : a portion of land nearly surrounded by water and connected with a larger body by an isthmus</strong>

<strong>2 : a piece of land jutting out into the water whether with or without a well-defined isthmus</strong>

<i>Synonyms: arm; cape; foreland; headland; ness; point; promontory; spit; </i>

***

<h3>subsistence</h3><strong>n. 生活；生存；存在</strong>

<strong>1 : real being : existence</strong>

<strong>2 : the condition of remaining in existence : continuation, persistence</strong>

<strong>3 : an essential characteristic quality of something that exists</strong>

<strong>4 : the character possessed by whatever is logically conceivable</strong>

<strong>5 : means of subsisting: such as</strong>

<strong>6 : the minimum (as of food and shelter) necessary to support life</strong>

<strong>7 : a source or means of obtaining the necessities of life</strong>

<i>Synonyms: actuality; corporality; corporeality; existence; reality; thingness; </i>

***

<h3>wailing</h3><strong>v. 哭泣（wail的ing形式）；哭号</strong>

<strong>1 : to express sorrow audibly : lament</strong>

<strong>2 : to make a sound suggestive of a mournful cry</strong>

<strong>3 : to express dissatisfaction plaintively : complain</strong>

<strong>4 : bewail</strong>

<strong>5 : to say or express plaintively

wailed that her cake was ruined</strong>

<i>Synonyms: beef; bellyache; bitch; bleat; carp; caterwaul; complain; crab; croak; fuss; gripe; grizzle; grouch; grouse; growl; grumble; grump; holler; inveigh; keen; kick; kvetch; maunder; moan; murmur; mutter; nag; repine; scream; squawk; squeal; whimper; whine; whinge; yammer; yawp; yaup; yowl; </i>

***

<h3>weeping</h3><strong>adj. （有些树木）有下垂枝条的；落泪的，哭泣的
v. 哭泣，悲痛；渗出（液体）（weep 的现在分词）</strong>

<strong>1 : tearful</strong>

<strong>2 : rainy</strong>

<strong>3 : having slender pendent branches

a weeping beech</strong>

<i>Synonyms: bowed; bowing; declined; declining; descendant; descendent; descending; drooping; droopy; hanging; hung; inclining; nodding; pendulous; sagging; stooping; </i>

***

<h3>contrarian</h3><strong>n. （尤指股票交易中的）逆向思维者
adj. 违反民意的</strong>

<strong>1 : a person who takes a contrary position or attitude</strong>

<strong>2 : an investor who buys shares of stock when most others are selling and sells when others are buying</strong>

***

<h3>garment</h3><strong>n. 衣服，服装；外表，外观
vt. 给…穿衣服
n. (Garment)人名；(英)加门特</strong>

<strong>1 : an article of clothing</strong>

<i>Synonyms: apparel; array; attire; bedeck; caparison; clothe; costume; deck (out); do up; dress; dress up; enrobe; garb; get up; gown; habit; invest; rig (out); robe; suit; tog (up or out); toilet; vesture; </i>

***

<h3>pastoralism</h3><strong>n. 牧歌体；田园主义</strong>

<strong>1 : the quality or style characteristic of pastoral writing</strong>

<strong>2 : livestock raising</strong>

<strong>3 : social organization based on livestock raising as the primary economic activity</strong>

***

<h3>procurement</h3><strong>n. 采购；获得，取得</strong>

<strong>1 : the act or process of procuring</strong>

<strong>2 : the obtaining of military supplies by a government</strong>

***

<h3>trite</h3><strong>adj. 陈腐的；平庸的；老一套的</strong>

<strong>1 : hackneyed or boring from much use : not fresh or original</strong>

<i>Synonyms: banal; cliché; cliche; clichéd; cobwebby; commonplace; hack; hackney; hackneyed; moth-eaten; musty; obligatory; shopworn; stale; stereotyped; threadbare; timeworn; tired; well-worn; </i>

***

<h3>denigrate</h3><strong>vt. 诋毁；使变黑；玷污</strong>

<strong>1 : to attack the reputation of : defame
denigrate one's opponents</strong>

<strong>2 : to deny the importance or validity of : belittle
denigrate their achievements</strong>

<i>Synonyms: bad-mouth; belittle; cry down; decry; deprecate; depreciate; derogate; diminish; dis; diss; discount; dismiss; disparage; kiss off; minimize; play down; poor-mouth; put down; run down; talk down; trash; trash-talk; vilipend; write off; </i>

***

<h3>rancorous</h3><strong>adj. 怀恶意的；深恨的</strong>

<strong>1 : marked by rancor : deeply malevolent

rancorous envy</strong>

<i>Synonyms: acrid; acrimonious; bitter; embittered; hard; resentful; sore; </i>

***

<h3>parley</h3><strong>n. 谈判；会谈
vi. 谈判；会谈
n. (Parley)人名；(英)帕利</strong>

<strong>1 : to speak with another : confer</strong>

<strong>2 : to discuss terms with an enemy</strong>

<i>Synonyms: advise; confab; confabulate; confer; consult; counsel; powwow; treat; </i>

***

<h3>immolate</h3><strong>vt. 献祭；牺牲；为供神而宰杀</strong>

<strong>1 : to offer in sacrifice</strong>

<strong>2 : to kill as a sacrificial victim</strong>

<strong>3 : to kill or destroy especially by fire</strong>

<i>Synonyms: offer; sacrifice; </i>

***

<h3>exterminate</h3><strong>vt. 消灭；根除</strong>

<strong>1 : to get rid of completely usually by killing off

exterminate termites and cockroaches</strong>

<i>Synonyms: abolish; annihilate; black out; blot out; cancel; clean (up); efface; eradicate; erase; expunge; extirpate; liquidate; obliterate; root (out); rub out; snuff (out); stamp (out); sweep (away); wipe out; </i>

***

<h3>strain</h3><strong>n. 张力；拉紧；负担；扭伤；血缘
vi. 拉紧；尽力
vt. 拉紧；滥用；滤去；竭力
n. (Strain)人名；(英)斯特兰
n. （植物、动物的）品种；种类</strong>

<strong>1 : lineage, ancestry</strong>

<strong>2 : a group of presumed common ancestry with clear-cut physiological but usually not morphological distinctions

a high-yielding strain of winter wheat</strong>

<strong>3 : a specified infraspecific group (such as a stock, line, or ecotype)</strong>

<strong>4 : kind, sort
discussions of a lofty strain</strong>

<strong>5 : inherited or inherent character, quality, or disposition

a strain of weakness in the family</strong>

<strong>6 : trace, streak
a strain of fanaticism</strong>

<strong>7 : tune, air</strong>

<strong>8 : a passage of verbal or musical expression</strong>

<strong>9 : a stream or outburst of forceful or impassioned speech</strong>

<strong>10 : the tenor, pervading note, burden, or tone of an utterance or of a course of action or conduct</strong>

<strong>11 : mood, temper</strong>

<i>Synonyms: pull; rack; stretch; wrench; </i>

***

<h3>parochial</h3><strong>adj. 教区的；狭小的；地方范围的</strong>

<strong>1 : of or relating to a church parish

our pastor and other parochial leaders</strong>

<strong>2 : of or relating to a parish as a unit of local government

parochial authorities serve the inhabitants of Louisiana's parishes</strong>

<strong>3 : confined or restricted as if within the borders of a parish : limited in range or scope (as to a narrow area or region) : provincial, narrow</strong>

<i>Synonyms: illiberal; insular; Lilliputian; little; narrow; narrow-minded; petty; picayune; provincial; sectarian; small; small-minded; </i>

***

<h3>posterity</h3><strong>n. 子孙，后裔；后代</strong>

<strong>1 : the offspring of one progenitor to the furthest generation</strong>

<strong>2 : all future generations</strong>

<i>Synonyms: fruit; get; issue; offspring; progeny; seed; spawn; </i>

***

<h3>levity</h3><strong>n. 多变；轻浮；轻率；不稳定</strong>

<strong>1 : excessive or unseemly frivolity

She would tolerate no levity in the classroom.</strong>

<strong>2 : lack of steadiness : changeableness
the levity … of the brisk fire— Charles Dickens</strong>

<i>Synonyms: facetiousness; flightiness; flippancy; frivolity; frivolousness; frothiness; light-headedness; light-mindedness; lightness; silliness; </i>

***

<h3>impecunious</h3><strong>adj. 没有钱的；贫穷的</strong>

<strong>1 : having very little or no money usually habitually : penniless</strong>

<i>Synonyms: beggared; beggarly; broke; destitute; dirt-poor; down-and-out; famished; hard up; impoverished; indigent; necessitous; needful; needy; pauperized; penniless; penurious; poor; poverty-stricken; skint; threadbare; </i>

***

<h3>multifarious</h3><strong>adj. 多种的；各式各样的；多方面的；多样性的</strong>

<strong>1 : having or occurring in great variety : diverse
participated in multifarious activities in high school</strong>

<i>Synonyms: divers; manifold; myriad; </i>

***

<h3>subdued</h3><strong>v. 制服，（使）屈服；抑制，克制（subdue 的过去式及过去分词）
adj. （人）闷闷不乐的；（光线或色彩）柔和的；（声音）压低的；（商业活动）冷清的，低迷的</strong>

<strong>1 : lacking in vitality, intensity, or strength

subdued colors</strong>

<i>Synonyms: conservative; low-key; low-keyed; muted; quiet; repressed; restrained; sober; toned-down; understated; unflashy; unpretentious; </i>

***

<h3>subdue</h3><strong>vt. 征服；抑制；减轻</strong>

<strong>1 : to conquer and bring into subjection : vanquish</strong>

<strong>2 : to bring under control especially by an exertion of the will : curb
subdued my foolish fears</strong>

<strong>3 : to bring (land) under cultivation</strong>

<strong>4 : to reduce the intensity or degree of : tone down</strong>

<i>Synonyms: beat; best; conquer; defeat; dispatch; do down; get; get around; lick; master; overbear; overcome; overmatch; prevail (over); skunk; stop; surmount; take; trim; triumph (over); upend; win (against); worst; </i>

***

<h3>haziness</h3><strong>n. 模糊，朦胧；混浊性，混浊度</strong>

<strong>1 : made dim or cloudy by or as if by fine dust, smoke, or light vapor in the air : obscured by or as if by haze (see haze entry 1 sense 1a)

hazy weather a hazy view of the mountains</strong>

<strong>2 : vague, indefinite
has only a hazy recollection</strong>

<strong>3 : uncertain</strong>

<i>Synonyms: beclouded; befogged; brumous; clouded; cloudy; foggy; gauzy; misty; murky; smoggy; soupy; </i>

***

<h3>exultant</h3><strong>adj. 非常高兴的；欢跃的；狂喜的；欢欣鼓舞的</strong>

<strong>1 : filled with or expressing great joy or triumph : jubilant
an exultant cheer exultant fans</strong>

<i>Synonyms: cock-a-hoop; crank; crowing; exulting; glorying; jubilant; prideful; proud; rejoicing; triumphant; </i>

***

<h3>reclusive</h3><strong>adj. 隐居的；隐遁的</strong>

<strong>1 : seeking solitude : retiring from society

a reclusive neighbor While Stephen King is far from reclusive, he doesn't spend much time promoting his horror-filled novels.— Sarah Lyall</strong>

<strong>2 : marked by seclusion or retirement : solitary
… sit under the reclusive calm of the acacia tree.— H. E. Bates</strong>

***

<h3>recollection</h3><strong>n. 回忆；回忆起的事物</strong>

<strong>1 : tranquility of mind</strong>

<strong>2 : religious contemplation</strong>

<strong>3 : the action or power of recalling to mind</strong>

<strong>4 : something recalled to the mind</strong>

<i>Synonyms: anamnesis; memory; recall; remembrance; reminiscence; </i>

***

<h3>poignant</h3><strong>adj. 尖锐的；辛酸的；深刻的；切中要害的
n. (Poignant)人名；(法)普瓦尼昂</strong>

<strong>1 : painfully affecting the feelings : piercing</strong>

<strong>2 : deeply affecting : touching</strong>

<strong>3 : designed to make an impression : cutting
poignant satire</strong>

<strong>4 : pleasurably stimulating</strong>

<strong>5 : being to the point : apt</strong>

<strong>6 : pungently pervasive
a poignant perfume</strong>

<i>Synonyms: affecting; emotional; impactful; impressive; moving; stirring; touching; </i>

***

<h3>sensationalized</h3><strong>vt. 以耸人听闻的手法处理；使引起轰动</strong>

<strong>1 : to present in a sensational manner</strong>

***

<h3>sanitized</h3><strong>v. 消毒；使清洁（sanitize的过去式）</strong>

<strong>1 : to make sanitary (as by cleaning or sterilizing)</strong>

<strong>2 : to make more acceptable by removing unpleasant or undesired features

attempts to sanitize historical accounts</strong>

***

<h3>spectacle</h3><strong>n. 景象；场面；奇观；壮观；盛大的演出；（复）眼镜</strong>

<strong>1 : something exhibited to view as unusual, notable, or entertaining</strong>

<strong>2 : an eye-catching or dramatic public display</strong>

<strong>3 : an object of curiosity or contempt

made a spectacle of herself</strong>

<strong>4 : a device used to correct defects of vision : glasses</strong>

<strong>5 : something (such as natural markings on an animal) suggesting a pair of glasses</strong>

<i>Synonyms: circus; extravaganza; pageant; raree-show; spectacular; </i>

***

<h3>distracted</h3><strong>v. （使）分心；转移（注意力）；（使）困惑（distract 的过去式及过去分词）
adj. 注意力分散的；心烦意乱的</strong>

<strong>1 : having one's thoughts or attention drawn away : unable to concentrate or give attention to something

a distracted driver … everyone else … is too distracted to see how lost and yearning she has become …— Richard Schickel … used to get so distracted by cellphone conversations overheard on the bus that she couldn't focus on reading.— Roni Caryn Rabin</strong>

<strong>2 : harassed, confused, or deranged especially by strong feelings

a mind distracted by grief … putting that poor gentleman through a course of questions that nearly drove him distracted.— Louisa May Alcott</strong>

<i>Synonyms: agitated; delirious; distrait; distraught; frantic; frenzied; hysterical; hysteric; </i>

***

<h3>inquisitive</h3><strong>adj. 好奇的；好问的，爱打听的</strong>

<strong>1 : given to examination or investigation</strong>

<strong>2 : inclined to ask questions</strong>

<strong>3 : inordinately or improperly curious about the affairs of others</strong>

<i>Synonyms: curious; nosy; nosey; prying; snoopy; </i>

***

<h3>intimidating</h3><strong>adj. 使人望而生畏的态度；令人紧张不安的；令人胆怯的</strong>

<strong>1 : causing a loss of courage or self-confidence : producing feelings of fear or timidity

an intimidating snarl a boss with a very intimidating manner The Red Army athletes were the most intimidating of competitors.— Ian Thomsen First jobs are intimidating for everyone, but few people have to cope with the stresses of the workplace at such a tender age as ballet dancers do.— Daphne Hurford</strong>

<i>Synonyms: alarming; dire; direful; dread; dreadful; fearful; fearsome; forbidding; formidable; frightening; frightful; ghastly; hair-raising; horrendous; horrible; horrifying; redoubtable; scary; shocking; spine-chilling; terrible; terrifying; </i>

***

<h3>dreary</h3><strong>adj. 沉闷的，枯燥的</strong>

<strong>1 : feeling, displaying, or reflecting listlessness or discouragement

cheer a dreary mind— George Berkeley</strong>

<strong>2 : having nothing likely to provide cheer, comfort, or interest : gloomy, dismal
a cold, dreary morning</strong>

<i>Synonyms: black; bleak; cheerless; chill; Cimmerian; cloudy; cold; comfortless; dark; darkening; depressing; depressive; desolate; dire; disconsolate; dismal; drear; dreich; elegiac; elegiacal; forlorn; funereal; gloomy; glum; godforsaken; gray; grey; lonely; lonesome; lugubrious; miserable; morbid; morose; murky; plutonian; saturnine; sepulchral; solemn; somber; sombre; sullen; sunless; tenebrific; tenebrous; wretched; </i>

***

<h3>scant</h3><strong>adj. 不足的；缺乏的；勉强够的
vt. 减少；节省；限制</strong>

<strong>1 : excessively frugal</strong>

<strong>2 : not prodigal : chary</strong>

<strong>3 : barely or scarcely sufficient</strong>

<strong>4 : not quite coming up to a stated measure</strong>

<i>Synonyms: exiguous; hand-to-mouth; light; meager; meagre; niggardly; poor; scanty; scarce; skimp; skimpy; slender; slim; spare; sparing; sparse; stingy; </i>

***

<h3>traction</h3><strong>n. 牵引；[机][车辆] 牵引力</strong>

<strong>1 : the adhesive friction of a body on a surface on which it moves

the traction of a wheel on a rail</strong>

<strong>2 : a pulling force exerted on a skeletal structure (as in a fracture) by means of a special device

a traction splint</strong>

<strong>3 : a state of tension created by such a pulling force</strong>

<strong>4 : the act of drawing : the state of being drawn</strong>

<strong>5 : the force exerted in drawing</strong>

<strong>6 : the support or interest that is needed for something to make progress or succeed

a product that is starting to gain traction among consumers … the litmus test issues of abortion and gay marriage have been losing traction, subordinated to the Iraq war and terrorism.— Thomas B. Edsall As many economists have noted, cutting spending is the worst thing people with means can do for the economy right now. But that argument seems to have little traction, especially because even those with steady paychecks and no fear of losing their job have seen their net worth decline and their retirement savings evaporate.— Shaila Dewan</strong>

<strong>7 : the drawing of a vehicle by motive power</strong>

<strong>8 : the motive power employed</strong>

***

<h3>garner</h3><strong>vt. 获得；储存；把…储入谷仓
n. 谷仓</strong>

<strong>1 : to gather into storage</strong>

<strong>2 : to deposit as if in a granary

volumes in which he has garnered the fruits of his lifetime labors— Reinhold Niebuhr</strong>

<strong>3 : to acquire by effort : earn
garnered much praise for his fundraising</strong>

<strong>4 : accumulate, collect</strong>

<i>Synonyms: accumulate; amass; assemble; bulk (up); collect; concentrate; congregate; constellate; corral; gather; group; lump; pick up; round up; </i>

***

<h3>oblivious</h3><strong>adj. 遗忘的；健忘的；不注意的；不知道的</strong>

<strong>1 : lacking remembrance, memory, or mindful attention</strong>

<strong>2 : lacking active conscious knowledge or awareness

—usually used with of or to</strong>

<i>Synonyms: clueless; ignorant; incognizant; innocent; insensible; nescient; unacquainted; unaware; unconscious; uninformed; unknowing; unmindful; unwitting; </i>

***

<h3>prerogative</h3><strong>n. 特权
adj. 有特权的</strong>

<strong>1 : an exclusive or special right, power, or privilege: such as</strong>

<strong>2 : one belonging to an office or an official body</strong>

<strong>3 : one belonging to a person, group, or class of individuals</strong>

<strong>4 : one possessed by a nation as an attribute of sovereignty</strong>

<strong>5 : the discretionary power inhering in the British Crown</strong>

<strong>6 : a distinctive excellence</strong>

<i>Synonyms: appanage; apanage; birthright; right; </i>

***

<h3>abet</h3><strong>vt. 煽动，教唆；支持
n. (Abet)人名；(意)阿贝特；(匈)奥拜特</strong>

<strong>1 : to actively second and encourage (something, such as an activity or plan)

abet the commission of a crime</strong>

<strong>2 : to assist or support (someone) in the achievement of a purpose

The singer was abetted by a skillful accompanist.</strong>

<strong>3 : to assist, encourage, instigate, or support with criminal intent in attempting or carrying out a crime</strong>

<i>Synonyms: brew; ferment; foment; incite; instigate; pick; provoke; raise; stir (up); whip (up); </i>

***

<h3>disposed</h3><strong>adj. 有…倾向的；打算做…的；有某种健康状态的
v. 处理；配置；使适应（dispose的过去分词）；使有倾向</strong>

<strong>1 : to give a tendency to : incline
faulty diet disposes one to sickness</strong>

<strong>2 : to put in place : set in readiness : arrange
disposing troops for withdrawal</strong>

<strong>3 : regulate</strong>

<strong>4 : bestow</strong>

<strong>5 : to settle a matter finally</strong>

<strong>6 : to come to terms</strong>

<strong>7 : to place, distribute, or arrange especially in an orderly way

disposing of the weapons in the new fort</strong>

<strong>8 : to transfer to the control of another

disposing of personal property to a total stranger</strong>

<strong>9 : to get rid of

how to dispose of toxic waste</strong>

<strong>10 : to deal with conclusively

disposed of the matter efficiently</strong>

<i>Synonyms: depose; deposit; emplace; fix; lay; place; position; put; set; set up; situate; stick; </i>

***

<h3>reinstate</h3><strong>vt. 使恢复；使复原</strong>

<strong>1 : to place again (as in possession or in a former position)</strong>

<strong>2 : to restore to a previous effective state</strong>

***

<h3>calumny</h3><strong>n. 诽谤；中伤；诬蔑</strong>

<strong>1 : a misrepresentation intended to harm another's reputation

denounced his opponent for his defamatory insinuations and calumny</strong>

<strong>2 : the act of uttering false charges or misrepresentations maliciously calculated to harm another's reputation

He was the target of calumny for his unpopular beliefs.</strong>

<i>Synonyms: aspersing; blackening; calumniation; character assassination; defamation; defaming; libel; libeling; libelling; maligning; slander; smearing; traducing; vilification; vilifying; </i>

***

<h3>blandishment</h3><strong>n. 奉承；谄媚；哄诱</strong>

<strong>1 : something that tends to coax or cajole : allurement

—often used in plural … refuses to yield to their blandishments …— Irving Babbitt</strong>

***

<h3>vituperation</h3><strong>n. 谩骂；辱骂；坏话</strong>

<strong>1 : sustained and bitter railing and condemnation : vituperative utterance</strong>

<strong>2 : an act or instance of vituperating</strong>

<i>Synonyms: abuse; billingsgate; fulmination; invective; obloquy; scurrility; vitriol; </i>

***

<h3>prurience</h3><strong>n. 淫乱；好色</strong>

<strong>1 : the quality or state of being prurient</strong>

***

<h3>defamation</h3><strong>n. 诽谤；中伤</strong>

<strong>1 : the act of communicating false statements about a person that injure the reputation of that person : the act of defaming another : calumny
defamation of character a defamation lawsuit</strong>

<i>Synonyms: aspersing; blackening; calumniation; calumny; character assassination; defaming; libel; libeling; libelling; maligning; slander; smearing; traducing; vilification; vilifying; </i>

***

<h3>hypostatize</h3><strong>vt. 使…具体化；使…实体化</strong>

<strong>1 : to attribute real identity to (a concept)</strong>

***

<h3>abjure</h3><strong>vt. 发誓放弃；公开放弃；避免</strong>

<strong>1 : to renounce upon oath

He abjured his allegiance to his former country.</strong>

<strong>2 : to reject solemnly

She abjured her old beliefs.</strong>

<strong>3 : to abstain from : avoid
abjure extravagance</strong>

<i>Synonyms: abnegate; forswear; foreswear; recant; renege; renounce; repeal; repudiate; retract; take back; unsay; withdraw; </i>

***

<h3>evince</h3><strong>vt. 表明，表示；引起</strong>

<strong>1 : to constitute outward evidence of</strong>

<strong>2 : to display clearly : reveal</strong>

<i>Synonyms: bespeak; betray; communicate; declare; demonstrate; display; expose; give away; manifest; reveal; show; </i>

***

<h3>mishap</h3><strong>n. 灾祸；不幸事故；晦气</strong>

<strong>1 : an unfortunate accident

The fire was a tragic mishap that could have been prevented.</strong>

<strong>2 : bad luck : misfortune
The ceremony proceeded without mishap.</strong>

<i>Synonyms: accident; casualty; mischance; </i>

***

<h3>philistine</h3><strong>n. 非利士人；不喜欢文化艺术的人；没有文化教养的人；平庸之辈
adj. 平庸的；不关心文化艺术的</strong>

<strong>1 : a native or inhabitant of ancient Philistia</strong>

<strong>2 : a person who is guided by materialism and is usually disdainful of intellectual or artistic values</strong>

<strong>3 : one uninformed in a special area of knowledge</strong>

***

<h3>dilettante</h3><strong>n. 业余爱好者；一知半解者
adj. 浅薄的；业余艺术爱好的
vi. （在艺术、科学等方面）浅尝辄止</strong>

<strong>1 : a person having a superficial interest in an art or a branch of knowledge : dabbler</strong>

<strong>2 : an admirer or lover of the arts</strong>

<i>Synonyms: cognoscente; connoisseur; </i>

***

<h3>desiccate</h3><strong>vi. 变干
vt. 使干；干贮</strong>

<strong>1 : to dry up

the desiccated land</strong>

<strong>2 : to preserve (a food) by drying : dehydrate
desiccated coconut</strong>

<strong>3 : to drain of emotional or intellectual vitality
… a charming little romance … not desiccated and compressed within the pages of a book— Elinor Wylie</strong>

<strong>4 : to become dried up

leaves desiccating in winter Lake Valencia has been … steadily desiccating for more than 200 years.— J. Platt Bradbury et al.</strong>

<i>Synonyms: castrate; damp; dampen; deaden; dehydrate; devitalize; enervate; geld; lobotomize; petrify; </i>

***

<h3>bourgeoisie</h3><strong>n. 资产阶级；中产阶级</strong>

<strong>1 : middle class
members of the bourgeoisie</strong>

<strong>2 : members of the middle class</strong>

<strong>3 : a class or group of people with social behavior and political views held to be influenced by private-property interest : a social order dominated by capitalists or bourgeois (see bourgeois entry 2 sense 2)</strong>

***

<h3>machination</h3><strong>n. 阴谋；诡计</strong>

<strong>1 : an act of machinating</strong>

<strong>2 : a scheming or crafty action or artful design intended to accomplish some usually evil end

backstage machinations … that have dominated the film industry— Peter Bogdanovich</strong>

<i>Synonyms: conspiracy; design; intrigue; plot; scheme; </i>

***

<h3>atrocious</h3><strong>adj. 凶恶的，残暴的</strong>

<strong>1 : extremely wicked, brutal, or cruel : barbaric
prisoners subjected to atrocious treatment</strong>

<strong>2 : appalling, horrifying
the atrocious weapons of modern war an atrocious accident</strong>

<strong>3 : utterly revolting : abominable
atrocious working conditions atrocious weather</strong>

<strong>4 : of very poor quality

atrocious handwriting</strong>

<i>Synonyms: appalling; awful; dreadful; frightful; ghastly; grisly; gruesome; grewsome; hideous; horrendous; horrible; horrid; horrific; horrifying; lurid; macabre; monstrous; nightmare; nightmarish; shocking; terrible; terrific; </i>

***

<h3>cogent</h3><strong>adj. 有说服力的，令人信服的；清晰的，合乎逻辑的</strong>

<strong>1 : appealing forcibly to the mind or reason : convincing
cogent evidence</strong>

<strong>2 : pertinent, relevant
a cogent analysis</strong>

<strong>3 : having power to compel or constrain
cogent forces</strong>

<i>Synonyms: compelling; conclusive; convincing; decisive; effective; forceful; persuasive; satisfying; strong; telling; </i>

***

<h3>vagary</h3><strong>n. 奇想；奇特行为</strong>

<strong>1 : an erratic, unpredictable, or extravagant manifestation, action, or notion</strong>

<i>Synonyms: bee; caprice; crank; fancy; freak; humor; kink; maggot; megrim; notion; vagrancy; whim; whimsy; whimsey; </i>

***

<h3>imperturbable</h3><strong>adj. 冷静的；泰然自若的</strong>

<strong>1 : marked by extreme calm, impassivity, and steadiness : serene</strong>

<i>Synonyms: nerveless; unflappable; unshakable; </i>

***

<h3>unruffled</h3><strong>adj. 平静的；不骚动的；镇定的
v. 使平整；变得平静（unruffle的过去分词）</strong>

<strong>1 : poised and serene especially in the face of setbacks or confusion</strong>

<strong>2 : not ruffled : smooth
unruffled water</strong>

<i>Synonyms: calm; collected; composed; cool; coolheaded; equal; level; limpid; peaceful; placid; possessed; recollected; sedate; self-composed; self-possessed; serene; smooth; together; tranquil; undisturbed; unperturbed; unshaken; untroubled; unworried; </i>

***

<h3>arch</h3><strong>n. 弓形，拱形；拱门
adj. 主要的
vt. 使…弯成弓形；用拱连接
vi. 拱起；成为弓形
n. (Arch)人名；(德)阿尔希；(英)阿奇</strong>

<strong>1 : a typically curved structural member spanning an opening and serving as a support (as for the wall or other weight above the opening)</strong>

<strong>2 : something resembling an arch in form or function</strong>

<strong>3 : either of two vaulted portions of the bony structure of the foot that impart elasticity to it</strong>

<strong>4 : something that has a curved shape like an arch

There was a slight arch in her eyebrows. an arch in the cat's back</strong>

<strong>5 : archway</strong>

<i>Synonyms: angle; arc; bend; bow; crook; curvature; curve; inflection; turn; wind; </i>

***

<h3>idyllic</h3><strong>牧歌的
田园诗的
悠闲的</strong>

<strong>1 : pleasing or picturesque in natural simplicity</strong>

<strong>2 : of, relating to, or being an idyll</strong>

***

<h3>lingering</h3><strong>adj. 拖延的，迟迟不结束的
v. 逗留；磨蹭，拖延；继续存留，缓慢消失；持续看或思考；苟延残喘（linger 的现在分词）</strong>

<strong>1 : to be slow in parting or in quitting something : tarry
fans lingered outside the door</strong>

<strong>2 : to remain existent although often waning in strength, importance, or influence

lingering doubts lingering odors</strong>

<strong>3 : to remain alive although gradually dying

was seriously ill, but lingered on for several months</strong>

<strong>4 : to be slow to act : procrastinate
He lingered in settling the estate in order to increase his fees.</strong>

<strong>5 : to move slowly : saunter
lingering homeward</strong>

<strong>6 : to pass (a period of time) slowly</strong>

<strong>7 : delay</strong>

<i>Synonyms: crawl; creep; dally; dawdle; delay; diddle; dillydally; drag; lag; loiter; lollygag; lallygag; mope; poke; shilly-shally; tarry; </i>

***

<h3>incipient</h3><strong>adj. 初期的；初始的；起初的；发端的</strong>

<strong>1 : beginning to come into being or to become apparent

an incipient solar system evidence of incipient racial tension</strong>

<i>Synonyms: aborning; budding; inceptive; inchoate; nascent; </i>

***

