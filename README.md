## GRE Answers

This repo aims to provide light and useful interactive reference_answer-checking system for GRE Vobal Test.

### Content
1. `AW` is short introduction about Argument/Issue writing.
2. `vocab` consists of a `test.html` as a dictionary page (youdao and Webster dictionary on the same page side-by-side).
   1. change the content `var vocabularies` with vocabulary list you prefer.
   2. open `test.html` by a browser (Chrome is prefered)
   3. Click `Next` or `Prev` button to navigate the list. ![1.png](1.png)
3. `vocab` also contains a `test.py` which is a simple Python (version $\geq$ 3.0+) script to collect useful information from Youdao/Webster dictionary and compose a Markdown File. It provides Chinese/English version of definition and synonyms of each vocab (from Webster). ![4.png](4.png)
4. materials contains three Vobal Test materals and two writing materials.
5. `answer_1250.csv`, `answer_reading_260.csv` and `fo_jiao_bei_kao.csv` are answers for the Vobal materials, correspondingly.

### How to Use
Tested on MacOS and Ubuntu18.04.

1. Make sure you have `node` installed. `npm install` at the root of this repo.
2. `node server.js` to start the server.
3. Open a browser and navigate to *http://localhost:8000/list*. You should be able to see the list of answers. Choose one to start from the first question. ![2.png](2.png)
4. If you want to start from a customized question, for example, `answer_1250`'s Q25, you can navigate to *http://localhost:8000/answer_1250/25* or simply change the `1` in *http://localhost:8000/answer_1250/1* to `25`. ![3.png](3.png)
5. The answers of each question is Gaussian_Blurred, click on the blur to see the answer. (Or modify the `server.js` for more customized behavior)

